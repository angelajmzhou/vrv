##
# Copyright (c) 2023 LupLab.
#
# SPDX-License-Identifier: AGPL-3.0-only
##

# A simple user program that implements reading/printing from/to the TTY device

# Constants
    .equ    NEWLN       '\n'
    .equ    TTY_PUT     100
    .equ    TTY_GET     101
    .equ    TTY_FLUSH   102

    .data
prompt:     .asciz "Enter your name: "
response_a: .asciz "Hello "
response_b: .asciz "!\n"
buffer:     .zero 0x1000

# Main function
    .text
    .globl main
main:
    addi    sp, sp, -2 * 4
    sw      ra, 4(sp)
    sw      s0, 0(sp)

    la      s0, buffer

    la      a0, prompt
    jal     printstr

    mv      a0, s0
    jal     readln

    la      a0, response_a
    jal     printstr
    mv      a0, s0
    jal     printstr
    la      a0, response_b
    jal     printstr

    li      a7, TTY_FLUSH
    ecall

    lw      ra, 4(sp)
    lw      s0, 0(sp)
    addi    sp, sp, 2 * 4
    ret


### printstr(char *str)
# Prints a null-terminated string, only exiting once all characters are in
#   the kernel's buffer.
# Arguments:
# - a0 <- address of first byte in string
###
printstr:
    mv      t0, a0
    li      a7, TTY_PUT
printstr_loop:
    lb      t1, 0(t0)
    beqz    t1, printstr_end
printstr_retry:
    mv      a0, t1
    ecall
    bltz    a0, printstr_retry
    addi    t0, t0, 1
    j printstr_loop
printstr_end:
    ret


### readln(char *buf, int size)
# Reads bytes from the TTY device until either a newline character has been read
#   or `size - 1` bytes have been read (filling the buffer).
# Will block until one of the above conditions is met.
# Arguments:
# - a0 <- address of first byte in string
# - a1 <- size of buffer
# Retruns:
# - a0 <- number of bytes read
###
readln:
    mv      t0, a0      # Save base address
    mv      t1, t0      # Pointer to current slot
    add     t2, a1, t0
    addi    t2, t2, -1  # base + size - 1 (leaves room for null-terminater)
    li      a7, TTY_GET
readln_loop:
    ecall
    bltz    a0, readln_loop
    sb      a0, 0(t1)
    addi    a0, a0, -1 * NEWLN
    beqz    a0, readln_end
    addi    t1, t1, 1
    beq     t1, t2, readln_end
    j readln_loop
readln_end:
    sb      zero, 0(t1)
    sub     a0, t1, t1  # Return number of bytes read
    ret