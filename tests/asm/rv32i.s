lb a6, 8(ra)
lh x12, (x19)
lw a0, 12(fp)
lbu a4, 2 + 0x80(x2)
lhu t6, -0x80(s4)
sb x14, 8 + 8(x2)
sh x12, -6(x1)
sw x11, 5
add x5, x6, x7
sub a0, t2, fp
sll s1, s2, s3
slt t1, t2, t3
sltu a1, a2, a3
xor x0, x4, x8
srl s4, s5, s6
sra t4, t5, t6
or a4, a5, a6
and x4, x9, x20
addi a5, ra, -50
slti a0, a1, 2 + 5
sltiu a0, a1, 2 - -5
xori a0, a1, (4 + 6) >> 1
ori t0, t2, 0x800
andi t0, t2, 0x7ff
slli x4, x8, 31
srli x5, x9, 10 + 5
srai x7, x1, 21
beq s3, a1, 0xf314
bne x19, x11, -3308
blt x20, x10, 0x1000
bge x21, x9, 0x0ffe
bltu x22, x8, -0x0fff - 1
bgeu x23, x7, - 0x1000
jal x5, 132
jalr a6, 24(t0)
auipc x3, -330097
lui x17, 116088
fence iorw, or
fence ori, w
ecall
ebreak
fence.i
csrrw x6, cycleh, x8
csrrs x4, FRM, x9
csrrc x2, instret, x10
csrrwi x15, cycle, 0x15
csrrsi x16, fcsr, 2
csrrci x17, fflags, 30
mul x7, x20, x10
mulh x8, x21, x9
mulhsu x9, x23, x8
mulhu x10, x24, x7
div t0, t1, t2
divu t2, t4, t6
rem a0, a1, a1
remu a2, a3, a5
flw f7, 100(x8)
fsw fa4, 100(s1)
fmadd.s f3, f7, f15, f31
fmsub.s f4, f8, f16, f0
fnmsub.s f2, f4, f8, f16
fnmadd.s f1, f2, f3, f4
fadd.s f5, f9, f17, dyn
fsub.s fa0, fa1, fa2
fmul.s ft0, ft1, ft2
fdiv.s ft3, ft4, ft5
fsqrt.s ft6, ft7
fsgnj.s ft8, ft9, fa1
fsgnjn.s ft10, ft11, fa2
fsgnjx.s f6, f12, f24
fmin.s fs0, fs1, fs2
fmax.s fs3, fs4, fs5
fcvt.w.s a0, fs6
fcvt.wu.s fp, f20
fmv.x.w a1, fs7
fclass.s a2, fs8
feq.s a3, fs9, fs10
flt.s t1, ft4, ft5
fle.s a4, fs11, fa7
fcvt.s.w fa6, a6
fcvt.s.wu fa5, a5
fmv.w.x fa1, a1

