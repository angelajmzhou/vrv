# Constant symbols
    .equ    PRINT_STR   4
    .equ    READ_STR    14
    .equ    BUFF_SIZE   16

    .equ    EXIT_COND1   0xa51 # Matches the input "Q\n"
    .equ    EXIT_COND2   0xa71 # Matches the input "q\n"

    .data
    .comm   buff BUFF_SIZE

    .text
    .globl main
main:
    la      a0, buff
    li      a1, BUFF_SIZE
    li      a4, EXIT_COND1
    li      a5, EXIT_COND2
loop:
    li      a7, READ_STR
    ecall
    li      a7, PRINT_STR
    ecall

    # Pack first two chars to check for exit condition
    lb      t0, 0(a0)
    lb      t1, 1(a0)
    slli    t1, t1, 8
    or      t0, t0, t1
    beq     t0, a4, exit
    beq     t0, a5, exit

    j       loop

exit:
    ret
