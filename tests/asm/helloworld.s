# Constant symbols
    .equ    PRINT_STR   4

    .data
helloworld_msg: .string "Hello World!\n"

    .text
    .globl main
main:
    la      a0, helloworld_msg
    li      a7, PRINT_STR
    ecall
    ret
