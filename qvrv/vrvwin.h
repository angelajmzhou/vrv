/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef VRVWIN_H
#define VRVWIN_H

#include <QMainWindow>
#include <QSettings>
#include <QList>
#include <QString>

#include "ui_vrvwin.h"
#include "console.h"
#include "fileloaddialog.h"
#include "regmodel.h"

#include "../emu/memseg.h"
#include "../emu/reg.h"

#define QVRV_RUN_STEPS 65536


namespace Ui {
    class VrvWindow;
}

class VrvWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit VrvWindow(QWidget *parent = nullptr);

    virtual void closeEvent(QCloseEvent *);

    void WriteOutput(QString message);
    void Error(QString message, bool fatal);
    void Log(QString message);

    bool InitializeWorld();

    void DisplayTextSegments(bool force);
    void UpdateDataDisplay();

    Ui::VrvWindow *ui;

    void SetOutputColor(QString color);

private:

    QString outputColor;

    // Program state
    QSettings settings;
    void readSettings();
    void writeSettings();
    QString stdExceptionHandler;

    // Register window
    GPRModel gprModel;
    FPRModel fprModel;
    CSRModel csrModel;

    // Text window
    bool st_showUserTextSegment;
    bool st_showKernelTextSegment;
    bool st_showTextComments;
    bool st_showTextDisassembly;
    QFont st_textWinFont;
    QColor st_textWinFontColor;
    QColor st_textWinBackgroundColor;

    // Dialogs
    FileLoadDialog *fileLoad_d;

    //
    // Enums:
    //

    enum ProgStatus {
        StatusIdle = 0,
        StatusError,
        StatusStopped,
        StatusPaused,
        StatusRunning,
        StatusSinglestep
    };
    ProgStatus programStatus;

    enum ExecActionState {
        ExecNo = 0,
        ExecRun,
        ExecPause,
    };

    //
    // Methods:
    //

    // Text segment window
    QString formatUserTextSeg();
    QString formatKernelTextSeg();
    QString formatInstructions(memseg_s *seg);
    void highlightInstruction(memaddr_t pc);

    //
    // Menu functions
    //
    void setupWindowGeometry();
    void setupDockWidgets();
    void setupMenus();
    void wireCommands();
    void initStack();
    void executeProgram(int steps);
    void updateStatus(ProgStatus status);
    void setExecActionState(ExecActionState state);

    //
    // Console
    //
public:
    Console* VRVConsole;
    bool waitingForConsoleInput;

    void setWaitingForConsoleInput(bool waiting);

public slots:
    void file_LoadFile();
    void file_SaveLogFile();

    void fileLoad_accepted();

    /* Execution */
    void execReset();
    void execRun();
    void execPause();
    void execSingleStep();

    void consoleHasLine();

    /* Text segment parameters */
    void textShowUser(bool isChecked);
    void textShowKernel(bool isChecked);
    void textShowComments(bool isChecked);
    void textShowEncoding(bool isChecked);

    void dataShowUser(bool isChecked);
    void dataShowStack(bool isChecked);
    void dataShowKernel(bool isChecked);
    void dataGrouped(bool isChecked);
    void dataBase(QAction *action);

    void about(void);

    void setDataDisplayBase(int base);
    void setVerbosityLevel(int verbosity);

    void tileDockWidgets();

    void continueBreakpoint();
    void singleStepBreakpoint();
    void abortBreakpoint();

signals:
    void updatedWorld(bool reset);
};

extern VrvWindow *Window;
extern QApplication *App;

#endif /* VRVWIN_H */
