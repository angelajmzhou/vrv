/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef TEXT_EDIT_H
#define TEXT_EDIT_H

#include <QAction>
#include <QMenu>
#include <QObject>
#include <QPlainTextEdit>
#include <QString>
#include <QTextCursor>
#include <QRegularExpression>

#include "format.h"

#include "../emu/emu.h"

/*
 * Derive QPlainTextEdit class so that we can have a custom context menu and
 * double clicking
 */
class TextEdit : public QPlainTextEdit
{
    Q_OBJECT

public:
    TextEdit(QWidget *parent = nullptr) : QPlainTextEdit(parent) {
        setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
        setContextMenuPolicy(Qt::CustomContextMenu);
        setReadOnly(true);
        viewport()->setCursor(Qt::ArrowCursor);
        connect(this, &QPlainTextEdit::customContextMenuRequested,
                this, &TextEdit::showContextMenu);
    }

    QString markBreakpoint = "<span style=\"color:red\">■</span>";

private:

    void toggleBreakpoint(const QPoint &pos) {
        QTextCursor cursor = cursorForPosition(pos);

        /* Get line under cursor */
        cursor.select(QTextCursor::LineUnderCursor);
        QString line = cursor.selectedText();

        /* Figure out PC for instruction */
        QRegularExpression regexpAddr("\\[([0-9a-fA-F]{8})\\]");
        QRegularExpressionMatch match = regexpAddr.match(line);

        /* Line under cursor doesn't contain an instruction */
        if (!match.hasMatch())
            return;

        uint pcAddr = match.captured(1).toUInt(nullptr, 16);

        /* Remove first character of line */
        cursor.setPosition(cursor.anchor());
        cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
        cursor.removeSelectedText();

        if (db_breakpoint_at_addr(pcAddr)) {
            /* Remove existing breakpoint */
            delete_breakpoint(pcAddr);

            /* Restore a space at the beginning of the line */
            cursor.insertHtml(genNbspHtml(1));
        } else {
            /* Add new breakpoint */
            add_breakpoint(pcAddr);

            /* Breakpoint icon */
            cursor.insertHtml(markBreakpoint);
        }
    }

protected:
    void mouseDoubleClickEvent(QMouseEvent *event) override {
        /* Don't call QPlainTextEdit::mouseDoubleClickEvent(event); to avoid
         * selecting text when double-clicking. Note that the user can still
         * select text when dragging the cursor */
        toggleBreakpoint(event->pos());
    }

public slots:
    void showContextMenu(const QPoint &pos) {
        QMenu contextMenu(this);
        QAction *action;

        action = contextMenu.addAction("Toggle breakpoint at this instruction");
        action->setData(pos);

        connect(action, &QAction::triggered,
                this, &TextEdit::actionToggleBreakpoint);

        contextMenu.exec(mapToGlobal(pos));
    }
    void actionToggleBreakpoint() {
        QAction *action = qobject_cast<QAction *>(sender());
        if (action)
            toggleBreakpoint(action->data().toPoint());
    }
};

#endif /* TEXT_EDIT_H */
