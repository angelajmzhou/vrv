/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include "vrvwin.h"

#include <QApplication>

#include "../emu/frontend.h"
#include "../emu/param.h"
#include "../emu/run.h"

#define BIG_BUF_SIZE 8192

/*
 * Front-end IO
 */
static bool console_input_available()
{
    return Window->VRVConsole->InputAvailable();
}

static void error(const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);

    char buf[BIG_BUF_SIZE];
    qvsnprintf(buf, BIG_BUF_SIZE, fmt, args);
    va_end(args);

    Window->SetOutputColor("red");
    Window->WriteOutput(buf);
    Window->SetOutputColor("");
}

static void run_error(const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);

    char buf[BIG_BUF_SIZE];
    qvsnprintf(buf, BIG_BUF_SIZE, fmt, args);
    va_end(args);

    Window->Error(buf, false);
}

static void fatal_error(const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);

    char buf[BIG_BUF_SIZE];
    qvsnprintf(buf, BIG_BUF_SIZE, fmt, args);
    va_end(args);

    Window->Error(buf, true);
}

static char get_console_char()
{
    return Window->VRVConsole->ReadChar();
}

static void put_console_char(char c)
{
    Window->VRVConsole->WriteOutput(QString(c));
    Window->VRVConsole->raise();
}

static bool read_input(char *str, int str_size)
{
    if (!Window->VRVConsole->ReadLine(str, str_size)) {
        Window->setWaitingForConsoleInput(true);
        return false;
    }
    return true;
}

static void write_output(const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);

    char buf[BIG_BUF_SIZE];
    qvsnprintf(buf, BIG_BUF_SIZE, fmt, args);
    va_end(args);

    if (ctx.state == EXEC_STATE) {
        Window->VRVConsole->WriteOutput(QString(buf));
    } else {
        Window->WriteOutput(buf);
    }
}

static void write_log(int vlvl, const char *fmt, ...)
{
    if (vlvl > ctl.verbose_lvl)
        return;

    va_list args;
    va_start (args, fmt);

    char buf[BIG_BUF_SIZE];
    qvsnprintf(buf, BIG_BUF_SIZE, fmt, args);
    va_end(args);

    Window->Log(buf);
}

static fe_api_s qvrv_fe = {
    .console_input_available = console_input_available,
    .get_console_char = get_console_char,
    .put_console_char = put_console_char,
    .read_input = read_input,
    .write_output = write_output,
    .write_log = write_log,
    .error = error,
    .run_error = run_error,
    .fatal_error = fatal_error,
};

/*
 * Main function
 */
int main(int argc, char *argv[])
{
    // Register frontend IO
    fe = &qvrv_fe;

    QApplication a(argc, argv);
    App = &a;

    VrvWindow w;
    w.show();

    return a.exec();
}
