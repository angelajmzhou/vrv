#ifndef STATE_H
#define STATE_H

#include <QString>

/* Defines library-exposed internal settings
 * - Most settings are still private to `VrvWindow` */

typedef struct qvrv_state {
    QString fileDialogDir;
} qvrv_state_s;

extern qvrv_state_s st;

#endif // STATE_H
