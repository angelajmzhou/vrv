/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef DATA_WIDGET_H
#define DATA_WIDGET_H

#include <QFlags>
#include <QPlainTextEdit>
#include <QString>
#include <QWidget>

#include "../emu/memseg.h"

class DataWidget : public QWidget
{
    Q_OBJECT

public:
    DataWidget();

    enum Segment {
        KernelDataSegment   = 0x1,
        UserDataSegment     = 0x2,
        UserStackSegment    = 0x4,

        AllDataSegments     = 0x7
    };
    Q_DECLARE_FLAGS(Segments, Segment);

    bool segmentVisibility(Segment segment);
    void setSegmentVisibility(Segment segment, bool visible);

    int base();
    void setBase(int base);

    bool groupedByWords(void);
    void setGroupedByWords(bool grouped);

    QString toPlainText();

public slots:
    void refreshView(bool reset);

private:
    QPlainTextEdit *text;
    Segments dataSegments;
    int dataBase;
    bool dataGrouped;
    uint memLineLen = BP_QWORD;

    void displaySegments(bool force);

    QString formatKernelDataSegment();
    QString formatUserDataSegment();
    QString formatUserStackSegment();

    QString formatSegment(QString title, uint segmentIndex);
    QString formatSegmentMemory(uint from, uint to);
    QString formatMemoryLineNum(uint from, uint to);
    QString formatMemoryLineChr(uint from, uint to);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DataWidget::Segments)

#endif /* DATA_WIDGET_H */
