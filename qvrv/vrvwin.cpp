/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include "vrvwin.h"
#include "ui_vrvwin.h"

#include <QStringBuilder>
#define QT_USE_FAST_CONCATENATION
#include <QMessageBox>
#include <QResource>
#include <QScreen>
#include <QScrollBar>
#include <QTemporaryFile>

#include "../emu/frontend.h"
#include "../emu/emu.h"
#include "../emu/run.h"

VrvWindow *Window;
QApplication *App;

VrvWindow::VrvWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VrvWindow),
    settings("qvrv", "qvrv"),
    fileLoad_d(new FileLoadDialog(this, &settings))
{
    Window = this;

    /* Set up UI created by QT creator */
    ui->setupUi(this);
    setupWindowGeometry();

    /* Set output log font */
    ui->outputLogWidget->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

    /* Set up and display dock widgets */
    setupDockWidgets();
    tileDockWidgets();

    /* Set up menus */
    setupMenus();

    // Wire up the menu and context menu commands
    wireCommands();

    // Restore program settings and window positions
#if 0
    /* Enable this to clear settings on next launch */
    settings.clear();
    settings.sync();
#endif
    readSettings();

    execReset();

    // Set program status
    programStatus = StatusIdle;
}

void VrvWindow::closeEvent(QCloseEvent *event)
{
    execPause();
    writeSettings();
    QMainWindow::closeEvent(event);
}


#define WINDOW_SIZE_FACTOR  0.9

void VrvWindow::setupWindowGeometry()
{
    /* Resize window to a certain portion of the screen */
    QRect screenGeo = QGuiApplication::primaryScreen()->geometry();
    QRect geo;
    geo.setRect(screenGeo.width() * (1.0 - WINDOW_SIZE_FACTOR) / 2.0,
                screenGeo.height() * (1.0 - WINDOW_SIZE_FACTOR) / 2.0,
                screenGeo.width() * WINDOW_SIZE_FACTOR,
                screenGeo.height() * WINDOW_SIZE_FACTOR);
    setGeometry(geo);
}

/* Only called once to set up the dock widgets */
void VrvWindow::setupDockWidgets()
{
    /* I/O console */
    VRVConsole = new Console(0);
    VRVConsole->setObjectName("Console");
    ui->consoleDockWidget->setWidget(VRVConsole);

    /* Connect register dock widgets and their respective models */
    ui->gprWidget->setRegModel(&gprModel);
    connect(this, &VrvWindow::updatedWorld, ui->gprWidget, &RegWidget::refreshView);
    ui->fprWidget->setRegModel(&fprModel);
    connect(this, &VrvWindow::updatedWorld, ui->fprWidget, &RegWidget::refreshView);
    ui->csrWidget->setRegModel(&csrModel);
    connect(this, &VrvWindow::updatedWorld, ui->csrWidget, &RegWidget::refreshView);

    /* Text/Data dock widgets */
    connect(this, &VrvWindow::updatedWorld, ui->textWidget, &TextWidget::refreshView);
    connect(this, &VrvWindow::updatedWorld, ui->dataWidget, &DataWidget::refreshView);

    /* Set style parameters for docking widgets */
    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::TopDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North);
}


#define REGS_DOCK_WIDTH_FACTOR      0.2
#define CONS_DOCK_WIDTH_FACTOR      0.4
#define SEGS_DOCK_HEIGHT_FACTOR     0.6

/* Tile and resize dock widget ratios to default */
void VrvWindow::tileDockWidgets()
{
    /* Remove floating from all the dock widgets */
    ui->gprDockWidget->setFloating(false);
    ui->fprDockWidget->setFloating(false);
    ui->csrDockWidget->setFloating(false);
    ui->textDockWidget->setFloating(false);
    ui->dataDockWidget->setFloating(false);
    ui->consoleDockWidget->setFloating(false);

    /* Show all the dock widgets */
    ui->gprDockWidget->show();
    ui->fprDockWidget->show();
    ui->csrDockWidget->show();
    ui->textDockWidget->show();
    ui->dataDockWidget->show();
    ui->consoleDockWidget->show();

    /* Left dock area */
    addDockWidget(Qt::LeftDockWidgetArea, ui->gprDockWidget);
    tabifyDockWidget(ui->gprDockWidget, ui->fprDockWidget);
    tabifyDockWidget(ui->fprDockWidget, ui->csrDockWidget);
    ui->gprDockWidget->raise();

    /* Top dock area */
    addDockWidget(Qt::TopDockWidgetArea, ui->textDockWidget);
    tabifyDockWidget(ui->textDockWidget, ui->dataDockWidget);
    ui->textDockWidget->raise();

    /* Right dock area */
    addDockWidget(Qt::RightDockWidgetArea, ui->consoleDockWidget);

    /* Resize dock widgets */
    QRect geo = geometry();
    resizeDocks(
        { ui->gprDockWidget, ui->consoleDockWidget },
        { (int)(geo.width() * REGS_DOCK_WIDTH_FACTOR),
          (int)(geo.width() * CONS_DOCK_WIDTH_FACTOR) },
        Qt::Horizontal);
    resizeDocks(
        { ui->textDockWidget },
        { (int)(geo.height() * SEGS_DOCK_HEIGHT_FACTOR) },
        Qt::Vertical);
}

void VrvWindow::setupMenus()
{
    /*
     * Text segment
     */

    /* Menu item for toggling dock */
    ui->textDockWidget->toggleViewAction()->setText("Text Segments");
    ui->menuTextView->insertAction(ui->menuTextView->actions().at(0),
                                   ui->textDockWidget->toggleViewAction());

    /* Disable additional options when dock is closed */
    connect(ui->textDockWidget, &QDockWidget::visibilityChanged, this,
            [=](bool isVisible) {
                Q_UNUSED(isVisible);
                bool b = !ui->textDockWidget->isHidden();
                ui->actionTextUser->setEnabled(b);
                ui->actionTextKernel->setEnabled(b);
                ui->actionTextComments->setEnabled(b);
                ui->actionTextEncoding->setEnabled(b);
            });

    /* Showing options */
    connect(ui->actionTextUser, SIGNAL(toggled(bool)), this, SLOT(textShowUser(bool)));
    connect(ui->actionTextKernel, SIGNAL(toggled(bool)), this, SLOT(textShowKernel(bool)));
    connect(ui->actionTextEncoding, SIGNAL(toggled(bool)), this, SLOT(textShowEncoding(bool)));
    connect(ui->actionTextComments, SIGNAL(toggled(bool)), this, SLOT(textShowComments(bool)));

    /*
     * Data segment
     */

    /* Menu item for toggling dock */
    ui->dataDockWidget->toggleViewAction()->setText("Data Segments");
    ui->menuDataView->insertAction(ui->menuDataView->actions().at(0),
                                   ui->dataDockWidget->toggleViewAction());

    /* Disable additional options when dock is closed */
    connect(ui->dataDockWidget, &QDockWidget::visibilityChanged, this,
            [=](bool isVisible) {
                Q_UNUSED(isVisible);
                bool b = !ui->dataDockWidget->isHidden();
                ui->actionDataUser->setEnabled(b);
                ui->actionDataStack->setEnabled(b);
                ui->actionDataKernel->setEnabled(b);
                ui->actionDataBase->setEnabled(b);
            });

    /* Showing options */
    connect(ui->actionDataUser, SIGNAL(toggled(bool)), this, SLOT(dataShowUser(bool)));
    connect(ui->actionDataStack, SIGNAL(toggled(bool)), this, SLOT(dataShowStack(bool)));
    connect(ui->actionDataKernel, SIGNAL(toggled(bool)), this, SLOT(dataShowKernel(bool)));

    /* Grouped option */
    connect(ui->actionDataGrouped, SIGNAL(toggled(bool)), this, SLOT(dataGrouped(bool)));

    /* Base options */
    ui->actionDataBase->setExclusive(true);
    ui->actionDataBin->setData(2);
    ui->actionDataDec->setData(10);
    ui->actionDataHex->setData(16);
    connect(ui->actionDataBase, &QActionGroup::triggered, this, &VrvWindow::dataBase);


    /*
     * Register docks
     */

    /* Menu items for toggling docks */
    ui->gprDockWidget->toggleViewAction()->setText("General-purpose Registers");
    ui->fprDockWidget->toggleViewAction()->setText("Floating-point Registers");
    ui->csrDockWidget->toggleViewAction()->setText("Control and Status Registers");
    ui->menuRegistersViews->addActions({
                                       ui->gprDockWidget->toggleViewAction(),
                                       ui->fprDockWidget->toggleViewAction(),
                                       ui->csrDockWidget->toggleViewAction()
                                       });

    /*
     * Exceptions settings
     */
    connect(ui->actionBreakExceptions, &QAction::toggled, this,
            [=](bool isChecked) { ctl.break_on_exceptions = isChecked; });
    connect(ui->actionExceptionsMisalignedLoads, &QAction::toggled, this,
            [=](bool isChecked) {
                if (isChecked)
                    ctl.misaligned_exceptions |= MEMOP_LOAD;
                else
                    ctl.misaligned_exceptions &= ~MEMOP_LOAD;
            });
    connect(ui->actionExceptionsMisalignedStores, &QAction::toggled, this,
            [=](bool isChecked) {
                if (isChecked)
                    ctl.misaligned_exceptions |= MEMOP_STORE;
                else
                    ctl.misaligned_exceptions &= ~MEMOP_STORE;
            });

    /*
     * Device settings
     */
    connect(ui->actionDeviceTTY, &QAction::toggled, this,
            [=](bool isChecked) {
                conf.mapped_tty = isChecked;
                ui->consoleDockWidget->setWindowTitle(isChecked
                    ? "Console (Memory-Mapped)"
                    : "Console");
                execReset();
            });

    /*
     * Verbosity settings
     */
    ui->actionVerbosity->setExclusive(true);
    ui->actionVerbosityQuiet->setData(0);
    ui->actionVerbosityNormal->setData(1);
    ui->actionVerbosityVerbose->setData(2);
    connect(ui->actionVerbosity, &QActionGroup::triggered, this,
            [=](QAction *action) {
                if (action) {
                    int verbosity = action->data().toInt();
                    ctl.verbose_lvl = verbosity;
                }
            });
}

void VrvWindow::wireCommands()
{
    QObject::connect(ui->action_File_Load, SIGNAL(triggered(bool)), this, SLOT(file_LoadFile()));
    QObject::connect(ui->action_File_SaveLog, SIGNAL(triggered(bool)), this, SLOT(file_SaveLogFile()));
    QObject::connect(ui->action_File_Exit, SIGNAL(triggered(bool)), this, SLOT(close()));

    QObject::connect(fileLoad_d, SIGNAL(accepted()), this, SLOT(fileLoad_accepted()));

    connect(ui->actionExecReset, &QAction::triggered, this, &VrvWindow::execReset);
    connect(ui->actionExecRun, &QAction::triggered, this, &VrvWindow::execRun);
    connect(ui->actionExecPause, &QAction::triggered, this, &VrvWindow::execPause);
    connect(ui->actionExecSingleStep, &QAction::triggered, this, &VrvWindow::execSingleStep);

    QObject::connect(VRVConsole, SIGNAL(inputReady()), this, SLOT(consoleHasLine()));

    QObject::connect(ui->actionRestoreViews, SIGNAL(triggered(bool)), this, SLOT(tileDockWidgets()));

    connect(ui->actionAbout, &QAction::triggered, this, &VrvWindow::about);
}

bool VrvWindow::InitializeWorld()
{
    bool useDefault = fileLoad_d->useDefaultSystemFile();
    if (!fileLoad_d->useDefaultSystemFile() && !QFile::exists(fileLoad_d->systemFilename())) {
        QMessageBox msgBox;
        msgBox.setText(QString("%1: system file not found.\n\nUsing default system file.")
                       .arg(fileLoad_d->systemFilename()));
        msgBox.exec();
        useDefault = true;
    }


    fe->write_log(1, "\n--- Memory and registers cleared ---\n");

    if (useDefault)
        return initialize_world(NULL);
    else
        // Use the file name supplied by the user.
        return initialize_world(fileLoad_d->systemFilename().toLocal8Bit().data());
}

void VrvWindow::UpdateDataDisplay()
{
    emit updatedWorld(false);
}

void VrvWindow::WriteOutput(QString out)
{
    /* Appending already adds a <br>, so avoid doubling last newline */
    if (out.endsWith("\n"))
        out.chop(1);

    QString outHtml = out
        .toHtmlEscaped()
        .replace("\n", "<br>")
        .replace(" ", "&nbsp;");

    // Determine if scroll area is at the bottom before appending
    QScrollBar *centralWidgetScroll = ui->outputLogWidget->verticalScrollBar();
    // Bias maximum for leniency
    bool scrollAtBottom = centralWidgetScroll
                          && centralWidgetScroll->value() >= centralWidgetScroll->maximum() - 4;

    ui->outputLogWidget->append(
        QString("<span "
            + (outputColor.size() ? "style=\"color:" + outputColor + "\"" : QString())
            + ">")
        + outHtml
        + QString("</span>")
    );

    // Keep scroll area bottomed-out if already at the bottom
    if (scrollAtBottom) {
        centralWidgetScroll->setValue(centralWidgetScroll->maximum());
    }
}

void VrvWindow::SetOutputColor(QString color)
{
    outputColor = color;
}

void VrvWindow::Error(QString message, bool fatal)
{
    SetOutputColor("red");
    WriteOutput(message);
    SetOutputColor("");

    if (fatal) {
        QMessageBox::critical(0, "Fatal error", message,
                              QMessageBox::Ok, QMessageBox::Ok);
        writeSettings();
        App->exit(1);
    } else {
        QMessageBox::StandardButton b =
            QMessageBox::information(0, "Error", message,
                                     QMessageBox::Ok | QMessageBox::Abort,
                                     QMessageBox::Ok);
        if (b == QMessageBox::Abort) {
            force_break = true;
        }
    }
}

void VrvWindow::Log(QString message)
{
    SetOutputColor("green");
    WriteOutput(message);
    SetOutputColor("");
}

