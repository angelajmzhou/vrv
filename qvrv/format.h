/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef FORMAT_H
#define FORMAT_H

#include <QString>

/*
 * Formatting helper functions
 */
QString format(uint val, int base, bool prefix, int width = 0);
QString formatAddress(uint addr);

QString formatByteHtml(uchar byte, int base);
QString formatWordHtml(uint word, int base);
QString formatCharHtml(char chr);
QString formatSegLabelHtml(QString segName, uint low, uint high);

QString genNbspHtml(int n);

#endif /* FORMAT_H */
