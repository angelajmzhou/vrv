#ifndef FILELOADDIALOG_H
#define FILELOADDIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QSettings>
#include <QTemporaryFile>
#include <QVBoxLayout>

class FileLoadDialog : public QDialog
{
    Q_OBJECT

private:
    QSettings *_settings;

    // Saved values
    bool _useDefaultSystemFile;
    QString _systemFilename;
    QStringList _programFilenames;
    QString _programArguments;

    // Internal widgets - system group
    QGroupBox *_systemFileLoadGroup;
    QCheckBox *_systemFileCheckbox;
    QLineEdit *_systemFileLoadValue;

    // Internal widgets - program files group
    QListWidget *_programFilenameList;
    QList<QPushButton *> _programFilenameSelectionButtons;

    // Internal widgets - program arguments
    QLineEdit *_programArgumentsLineEdit;

    // Internal widgets - dialog button box
    QDialogButtonBox *_dialogButtonBox;

public:
    FileLoadDialog(QWidget *parent = nullptr, QSettings *settings = nullptr);
    virtual ~FileLoadDialog() = default;

    virtual void showEvent(QShowEvent *event) override;

    void renderValues(bool resetToSavedValue);

    const QStringList &programFilenames() const;
    bool useDefaultSystemFile() const;
    const QString &systemFilename() const;
    const QString &programArguments() const;

public slots:
    void modifiedVoid();
    void modifiedBool(bool);
    void modifiedInt(int);

    void dialogButtonBoxClicked(QAbstractButton *button);

    void systemFileDialogOpen();

    void programFilenameAdd(bool);
    void programFilenameTop(bool);
    void programFilenameRemove(bool);
    void programFilenameClear(bool);

private:
    void readSettings();
    void writeSettings() const;
};

#endif // FILELOADDIALOG_H
