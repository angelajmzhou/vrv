/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QFont>
#include <QFontDatabase>
#include <QHeaderView>
#include <QTableWidgetItem>
#include <QVBoxLayout>

#include "regmodel.h"
#include "regwidget.h"

RegWidget::RegWidget()
{
    /* Font configuration */
    QFont font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    int lineHeight = QFontMetrics(font).height() * (float)1.1;

    /* Read-only table */
    QStringList labels = { "No.", "Name", "Value" };
    table = new QTableWidget(0, labels.length());
    table->setFont(font);
    table->setHorizontalHeaderLabels(labels);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    /* Put table in vertical layout
     * Makes the table resize along with the widget */
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(table);
    setLayout(layout);

    /* General table properties */
    table->setTabKeyNavigation(false);
    table->setDropIndicatorShown(false);
    table->setAlternatingRowColors(true);
    //table->setSelectionMode(QAbstractItemView::NoSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    table->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
    table->setCornerButtonEnabled(false);

    /* Columns configuration */
    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()->setHighlightSections(false);
    table->horizontalHeader()->setDefaultSectionSize(lineHeight);

    /* Rows configuration */
    table->verticalHeader()->setVisible(false);
    table->verticalHeader()->setHighlightSections(false);
    table->verticalHeader()->setMinimumSectionSize(lineHeight);
    table->verticalHeader()->setDefaultSectionSize(lineHeight);
}

void RegWidget::setRegModel(RegModel *model)
{
    this->model = model;

    table->setRowCount(model->length());

    /* Initialize contents */
    for (int i = 0; i < model->length(); i++) {
        table->setItem(i, 0, new QTableWidgetItem(model->number(i)));
        table->setItem(i, 1, new QTableWidgetItem(model->name(i)));
        table->setItem(i, 2, new QTableWidgetItem(model->value(i)));
    }

    table->resizeColumnsToContents();
}

void RegWidget::refreshView(bool reset)
{
    /* Refresh contents */
    for (int i = 0; i < model->length(); i++) {
        QTableWidgetItem *item = table->item(i, 2);

        QString val = model->value(i);
        bool changed = (val != item->text());

        if (changed)
            item->setText(val);

        if (changed && !reset)
            item->setForeground(QBrush(Qt::red));
        else
            item->setForeground(QBrush());
    }

    if (reset)
        table->scrollToTop();
}
