/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef CONSOLE_H
#define CONSOLE_H

#include <QObject>
#include <QPlainTextEdit>
#include <QString>
#include <QEventLoop>

class Console : public QPlainTextEdit
{
    Q_OBJECT

public:
    Console(QWidget* parent = nullptr);

    virtual void keyPressEvent(QKeyEvent *e);
    virtual void keyReleaseEvent(QKeyEvent *e);

    void Clear();
    void StopWaitForInput();
    bool InputAvailable();
    char ReadChar();
    bool ReadLine(char *buff, size_t size);
    void WriteOutput(QString out);

private:
    QString inputBuffer;
    int bufferLineCount;

signals:
    void inputReady();
};

#endif /* CONSOLE_H */
