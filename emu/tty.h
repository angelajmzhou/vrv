/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef TTY_H
#define TTY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "memloader.h"
#include "memseg.h"


/*
 * Macros
 */

/* Register offsets */
#define TTY_WRIT_OFFSET  (0 * BP_WORD)
#define TTY_READ_OFFSET  (1 * BP_WORD)
#define TTY_CTRL_OFFSET  (2 * BP_WORD)
#define TTY_STAT_OFFSET  (3 * BP_WORD)

/* Register addresses */
#define TTY_ADDR_BOT    K_DEVICE_BOT                    /* 0xffff0000 */
#define TTY_ADDR_TOP    (TTY_ADDR_BOT + 4 * BP_WORD)    /* 0xffff0010 */


/*
 * Static inline functions
 */

static inline bool addr_in_tty_range(memaddr_t addr, int num_bytes)
{
    return TTY_ADDR_BOT <= addr && addr + num_bytes <= TTY_ADDR_TOP;
}


/*
 * Exported functions
 */

/* Management */
void initialize_tty(void);

/* Access */
memword_t tty_read(memaddr_t offset, int num_bytes, access_level_e al);
void tty_write(memaddr_t offset, memword_t data, int num_bytes, access_level_e al);

/* Polling */
/***
 * Updates the tty device, checking to see if a character should be received
 *   and/or transmitted.
 * May raise interrupt bits directly into `MIP`.
 ***/
void check_tty_device(void);


#ifdef __cplusplus
}
#endif

#endif  // TTY_H
