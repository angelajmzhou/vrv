/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef MEMSEG_H
#define MEMSEG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/*
 * Typedefs
 */

typedef uint32_t memword_t;
typedef uint32_t memaddr_t;


/*
 * Macros
 */

/* xlen and width macros */
#define XLEN                32
#define BP_QWORD            16
#define BP_DWORD            8
#define BP_WORD             4
#define BP_HALF             2
#define BP_BYTE             1

/* Memory Privileges */
#define MEMSEG_READONLY     0x1     /* Read-Only when set */
#define MEMSEG_M_MODE       0x2     /* M-Mode when set, U-mode when cleared */
#define MEMSEG_FIXED        0x4     /* Memory segment cannot be expanded */

#define ALIGN_UP(addr, bytes) \
    (((addr) + (bytes) - 1) & ~((bytes) - 1))

#define ALIGN_DOWN(addr, bytes) \
    ((addr) & ~((bytes) - 1))


/*
 * Enums
 */

typedef enum access_level {
    U_MODE_ACCESS = 0,
    M_MODE_ACCESS,
    EMU_ACCESS,
} access_level_e;


/*
 * Structs
 */

typedef struct memseg {
    int8_t *data;           // Array of memory bytes
    uint32_t size;          // Size of allocated `data` in bytes
    memaddr_t lower_bound;  // Address of lower-bound, inclusive
    memaddr_t upper_bound;  // Address of upper-bound, exclusive
    memaddr_t loc_counter;  // Next memory free address relative to lower_bound
    uint8_t priv;           // Mask of memory privelages
    bool grows_down:1;      // For stacks, address space expands downards
    bool modified:1;        // True => segment has been modified, may be cleared externally
} memseg_s;


/*
 * Inline functions
 */

/* Address Utility */

/* Memoory Segment Utility */
static inline bool memseg_contains_range(memseg_s *seg, memaddr_t addr, uint32_t bytes)
{
    return addr >= seg->lower_bound && addr + bytes <= seg->upper_bound;
}

static inline bool memseg_can_read(memseg_s *seg, access_level_e access_lvl)
{
    return access_lvl || !(seg->priv & MEMSEG_M_MODE);
}

static inline bool memseg_can_write(memseg_s *seg, access_level_e access_lvl)
{
    // Can read and not read-only, or in machine mode with user-mode segment
    return access_lvl == EMU_ACCESS
        || (memseg_can_read(seg, access_lvl) && !(seg->priv & MEMSEG_READONLY))
        || (access_lvl == M_MODE_ACCESS && !(seg->priv & MEMSEG_M_MODE));
}


/*
 * Exported functions
 */

/* Initialization + Destruction */
void memseg_init(memseg_s *seg, bool grows_down, memaddr_t anchor_bound,
                 uint32_t size, uint8_t priv);
void memseg_destroy(memseg_s *seg);

/* Management */
memaddr_t memseg_align(memseg_s *seg, uint32_t byte_boundary);
void memseg_clear(memseg_s *seg);
void memseg_set_size(memseg_s *seg, uint32_t new_size);

/* Reading */
bool memseg_read_byte(memseg_s *seg, memaddr_t addr, int8_t *data);
bool memseg_read_half(memseg_s *seg, memaddr_t addr, int16_t *data);
bool memseg_read_word(memseg_s *seg, memaddr_t addr, int32_t *data);

/* Writing */
memaddr_t memseg_append_byte(memseg_s *seg, int8_t data);
memaddr_t memseg_append_half(memseg_s *seg, int16_t data);
memaddr_t memseg_append_word(memseg_s *seg, int32_t data);
bool memseg_write_byte(memseg_s *seg, memaddr_t addr, int8_t data);
bool memseg_write_half(memseg_s *seg, memaddr_t addr, int16_t data);
bool memseg_write_word(memseg_s *seg, memaddr_t addr, int32_t data);

#ifdef __cplusplus
}
#endif

#endif  // MEMSEG_H
