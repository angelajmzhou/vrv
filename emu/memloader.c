/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <stdbool.h>
#include <stdio.h>

#include "frontend.h"
#include "memloader.h"
#include "utils.h"


/*
 * Internal variables
 */

/* Memory loader state */
static memseg_s *seg;
static seg_idx_e seg_i;


/*
 * Exported variable definitions
 */
memseg_s M[MEMORY_SEG_COUNT];


/* ========================================================================== *
 * Main memory management
 * ========================================================================== */

void clear_memory(void)
{
    for (int i = 0; i < MEMORY_SEG_COUNT; ++i)
        memseg_clear(&M[i]);
}

void initialize_memory(void)
{
    /* Init segments */
    memseg_init(&M[TEXT_SEG],       false, TEXT_BOT,       DEFAULT_SEG_SIZE,    MEMSEG_READONLY);
    memseg_init(&M[SMALL_DATA_SEG], false, SMALL_DATA_BOT, SMALL_DATA_SIZE,     MEMSEG_FIXED);
    memseg_init(&M[DATA_SEG],       false, DATA_BOT,       DEFAULT_SEG_SIZE,    0);
    memseg_init(&M[K_TEXT_SEG],     false, K_TEXT_BOT,     DEFAULT_SEG_SIZE,    MEMSEG_M_MODE | MEMSEG_READONLY);
    memseg_init(&M[K_DATA_SEG],     false, K_DATA_BOT,     DEFAULT_SEG_SIZE,    MEMSEG_M_MODE);

    memseg_init(&M[STACK_SEG],      true,  STACK_TOP,      DEFAULT_SEG_SIZE, 0);

    /* Init memory loader state */
    set_segment(TEXT_SEG);
}

void destroy_memory(void)
{
    for (int i = 0; i < MEMORY_SEG_COUNT; ++i) {
        memseg_destroy(&M[i]);
    }
}


/* ========================================================================== *
 * Memory loader operations
 * ========================================================================== */

memaddr_t loader_align(uint32_t byte_boundary)
{
    return memseg_align(seg, byte_boundary);
}

seg_idx_e set_segment(seg_idx_e segment_index)
{
    seg_idx_e old_seg_i = seg_i;
    seg_i = segment_index;
    seg = &M[seg_i];
    return old_seg_i;
}


/* ========================================================================== *
 * Memory loader information
 * ========================================================================== */

int current_segment_index(void)
{
    return seg_i;
}


/* ========================================================================== *
 * Memory storing operations
 * ========================================================================== */
/* -------------------------------------------------------------------------- *
 * VRV ensures that all `store_` operations succeed. There should be enough
 * address space for normal use, so every other system using `memloader` can
 * guarantee that store operations will work or the program will fail.
 * -------------------------------------------------------------------------- */

memaddr_t store_byte(int8_t data)
{
    // Attempt to append byte
    memaddr_t addr = memseg_append_byte(&M[seg_i], data);

    // If append failed, attempt to expand segment
    if (!addr) {
        expand_memory(seg_i);
        addr = memseg_append_byte(&M[seg_i], data);
        if (!addr)
            fe->fatal_error(
                "Failed storing byte. Memory segment (%d) full and cannot expand further\n",
                seg_i);
    }

    return addr;
}

memaddr_t store_half(int16_t data)
{
    // Attempt to append halfword
    memaddr_t addr = memseg_append_half(&M[seg_i], data);

    // If append failed, attempt to expand segment
    if (!addr) {
        expand_memory(seg_i);
        addr = memseg_append_half(&M[seg_i], data);
        if (!addr)
            fe->fatal_error(
                "Failed storing halfword. Memory segment (%d) full and cannot expand further\n",
                seg_i);
    }

    return addr;
}

memaddr_t store_word(int32_t data)
{
    // Attempt to append word
    memaddr_t addr = memseg_append_word(&M[seg_i], data);

    // If append failed, attempt to expand segment
    if (!addr) {
        expand_memory(seg_i);
        addr = memseg_append_word(&M[seg_i], data);
        // Append still failed, segment must have reached max capacity, fatal error
        if (!addr)
            fe->fatal_error(
                "Failed storing word. Memory segment (%d) full and cannot expand further\n",
                seg_i);
    }

    return addr;
}


/* ========================================================================== *
 * Memory segment operations (not necessarily used during loading)
 * ========================================================================== */

/* Expand given memory segment by either EXPANSION_FACTOR or up to next segment.
 * - Returns number of bytes added */
int32_t expand_memory(seg_idx_e segment_index)
{
    return memory_sbrk(segment_index,
                       EXPANSION_FACTOR(M[segment_index].size));
}

/* Expand given memory segment by either EXPANSION_FACTOR or up to next segment.
 * - If expanded size exceeds max, then the size is clamped to max.
 * - Returns number of bytes added */
int32_t expand_memory_clamped(seg_idx_e segment_index, uint32_t max_size)
{
    uint32_t segment_size = M[segment_index].size;
    if (max_size <= segment_size)
        return 0;
    int32_t byte_delta = MIN(max_size - segment_size, EXPANSION_FACTOR(segment_size));
    return memory_sbrk(segment_index, byte_delta);
}

int32_t memory_sbrk(seg_idx_e segment_index, int32_t byte_delta)
{
    if (byte_delta == 0)
        return 0;

    memseg_s *segment = &M[segment_index];

    // Prevent expansion of fixed segments
    if (segment->priv & MEMSEG_FIXED)
        return 0;

    uint32_t new_size = segment->size;

    if (byte_delta < 0) {
        // Shrink segment towards 0
        new_size -= MIN(new_size, (uint32_t)-byte_delta);

    } else if (!segment->grows_down) {
        // Expand upwards (text and data segments)
        memaddr_t max_upper_bound = segment_index + 1 < MEMORY_SEG_COUNT
            ? (segment + 1)->lower_bound - SEG_GUARD    // Bottom of next segment - SEG_GUARD
            : (memaddr_t)-SEG_GUARD;                    // Top of address space - SEG_GUARD
        if (max_upper_bound <= segment->upper_bound) {
            // No room for expansion
            return 0;
        }

        new_size += MIN(max_upper_bound - segment->upper_bound, (uint32_t)byte_delta);

    } else {
        // Expand downwards (stack)
        memaddr_t min_lower_bound = segment_index > 0
            ? (segment - 1)->upper_bound + SEG_GUARD    // Top of previous segment + SEG_GUARD
            : SEG_GUARD;                                // Bottom of address space + SEG_GUARD
        if (min_lower_bound >= segment->lower_bound) {
            // No room for expansion
            return 0;
        }

        new_size += MIN(segment->lower_bound - min_lower_bound, (uint32_t)byte_delta);
    }

    byte_delta = (int32_t)new_size - (int32_t)segment->size;
    memseg_set_size(segment, new_size);
    return byte_delta;
}
