/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Exported functions
 */

#ifndef PARSER_H
#define PARSER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "memseg.h"

void fix_current_label_address(memaddr_t new_addr);
void initialize_parser(char *file_name);
void yyerror(char *s);
int yyparse(void);

#ifdef __cplusplus
}
#endif

#endif  // PARSER_H
