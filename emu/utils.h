/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef UTILS_H
#define UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


/*
 * Macros
 */

#define K 1024

#define streq(s1, s2) !strcmp(s1, s2)

#ifdef MIN                      /* Some systems define these in system includes */
#undef MIN
#endif
#ifdef MAX
#undef MAX
#endif
#define MIN(a, b)       ((a) < (b) ? (a) : (b))
#define MAX(a, b)       ((a) > (b) ? (a) : (b))

#define CEIL(a, b)      (((a) + (b) - 1) / (b))
#define LSB(x)          ((x) & ~((x) - 1))

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*(a)))

#define memclr(b, s) memset((void*)b, 0, s)


/*
 * Unions
 */

/* Union for easy access to floating-point bit representation */
typedef union floatbits {
    uint32_t b;
    float f;
} floatbits_u;


/*
 * Exported functions
 */

char *filepath_noext(char *path);
int strcmp_ci(const char *a, const char *b);
bool str_is_ciprefix(const char *a, const char *b, int min_match);
char *xstrdup(const char *str);
char *xstrdup_tolower(const char *str);
void *xmalloc(int);
void *xmallocz(int);
void *xrealloc(void *src, size_t size);
void *xreallocz(void *src, size_t size, size_t old_size);

#ifdef __cplusplus
}
#endif

#endif  // UTILS_H
