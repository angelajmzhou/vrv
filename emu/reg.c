/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "csr.h"
#include "emu.h"
#include "memloader.h"
#include "reg.h"
#include "symbol.h"
#include "utils.h"

regword_t R[R_LENGTH];
memaddr_t PC;
fregword_t FR[FR_LENGTH];

char *gpr_names[] = {
    "x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11",
    "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22",
    "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31"
};

char *abi_gpr_names[] = {
    "zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0", "s1", "a0", "a1",
    "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7",
    "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6"
};

char *fpr_names[] = {
    "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11",
    "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20", "f21", "f22",
    "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30", "f31"
};

char *abi_fpr_names[] = {
    "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7", "fs0", "fs1", "fa0",
    "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7", "fs2", "fs3", "fs4", "fs5",
    "fs6", "fs7", "fs8", "fs9", "fs10", "fs11", "ft8", "ft9", "ft10", "ft11"
};

void initialize_registers(void)
{
    // Clear all registers
    memclr(R, sizeof(R));
    memclr(FR, sizeof(FR));
    memclr(C, CSR_ARRAY_SIZE * sizeof(uregword_t));

    // Set `sp`, `gp`,  `pc`
    R[REG_SP] = STACK_TOP;
    R[REG_GP] = SMALL_DATA_MID;
    // - Function returns 0 if symbol is undefined
    PC = find_symbol_address(DEFAULT_RUN_LOCATION);
}
