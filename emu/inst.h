/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef INST_H
#define INST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdlib.h>

#include "memseg.h"
#include "sstream.h"


/*
 * Typedefs
 */

typedef int8_t reg_t;
typedef uint16_t opc_t;
typedef int32_t imm_t;
typedef uint32_t uimm_t;
typedef uint32_t instword_t;


/*
 * Structs
 */

/* Represenation of the expression that produce a value for an instruction's
 * immediate field.  Immediates have the form: label +/- offset. */
typedef struct immexpr {
    imm_t offset;               // Offset from symbol
    imm_t mask;                 // Mask determining which bits to use
    struct label *symbol;       // Symbolic label
    uint8_t width;              // Bit width of the field
    uint8_t shift;              // Amount to shift value by on eval
    reg_t base_reg;             // Base register for mem access (-1 for pure immexpr)
    bool pc_relative:1;         // Interpret offset relative to address of instruction
    bool uimm:1;                // Evaluate immediate as unsigned
    bool neg_symbol:1;          // Negate symbol value on eval
    bool hi_part:1;             // Interpret as high part of immediate
    bool lo_part:1;             // Interpret as low part of immediate
} immexpr_s;

/* Representation of an instruction. */
typedef struct inst {
    char *source_line;
    immexpr_s *expr;
    instword_t encoding;
    memaddr_t addr;
    opc_t opcode;

    reg_t rd;
    reg_t rs1;
    imm_t rs2;      // `imm_t` in order to fit `csr` value in `Zicsr` insts
    reg_t rs3;
    imm_t imm;
} inst_s;


/*
 * Macros
 */

/* Macros for `immexpr_s->shift` field */
#define IMMEXPR_SHIFT_MAX   0x1f
#define IMMEXPR_SHIFT_LEFT  0x20
#define IMMEXPR_SHIFT_LOGIC 0x40

/* Fields in binary representation of instructions: */
#define SHIFT_RD             7
#define SHIFT_FUNCT3        12
#define SHIFT_RS1           15
#define SHIFT_RS2           20
#define SHIFT_FUNCT7        25
#define SHIFT_RS3           27

#define ENC_FIELD(b,l,o)    (((b) & ((1 << (l)) - 1)) << (o))

#define ENC_FUNCT3(b)       ENC_FIELD(b, 3, SHIFT_FUNCT3)
#define ENC_FUNCT7(b)       ENC_FIELD(b, 7, SHIFT_FUNCT7)

#define ENC_REG(r,o)        ENC_FIELD(r, 5, o)
#define ENC_RD(r)           ENC_REG(r, SHIFT_RD)
#define ENC_RS1(r)          ENC_REG(r, SHIFT_RS1)
#define ENC_RS2(r)          ENC_REG(r, SHIFT_RS2)
#define ENC_RS3(r)          ENC_REG(r, SHIFT_RS3)

#define DEC_FIELD(b,l,o)    (((b) >> (o)) & ((1 << (l)) - 1))

#define DEC_REG(r,o)        DEC_FIELD(r, 5, o)
#define DEC_RD(r)           DEC_REG(r, SHIFT_RD)
#define DEC_RS1(r)          DEC_REG(r, SHIFT_RS1)
#define DEC_RS2(r)          DEC_REG(r, SHIFT_RS2)
#define DEC_RS3(r)          DEC_REG(r, SHIFT_RS3)

#define DEC_FUNCT3(b)       DEC_FIELD(b, 3, SHIFT_FUNCT3)
#define DEC_FUNCT7(b)       DEC_FIELD(b, 7, SHIFT_FUNCT7)

#define MASK_FIELD(b,l,o)   ((b) & (((1 << (l)) - 1) << (o)))

#define MASK_FUNCT3(b)      MASK_FIELD(b, 3, SHIFT_FUNCT3)
#define MASK_RS2(b)         MASK_FIELD(b, 5, SHIFT_RS2)
#define MASK_FUNCT7(b)      MASK_FIELD(b, 7, SHIFT_FUNCT7)

/* Minimum and maximum values that fit in instruction's imm field */
#define UIMM_MAX(l)         (uimm_t)(l >= 32 ? 0xffffffff : ((uimm_t)1UL << (l)) - 1)
#define UIMM_MIN            (uimm_t)0

#define IMM_MAX(l)          (imm_t)UIMM_MAX((l) - 1)
#define IMM_MIN(l)          ~IMM_MAX(l)


/*
 * Static inline functions
 */

/* Sign-extend the binary value in the bottom `len` bits of `val` to 32-bits.
 * Used for creating internal repressentations of provided immediate values,
 *   as well as to extend sub-word loads into word-sized registers.
 * Test the sign bit of `b` (bit `l`):
 * - If signed, set all bits above signed bit
 *   - `IMM_MIN` creates 32-bit signed minimum value `l`-lengthed binary fields.
 * - If unsigned, mask out everything but the lower `l` bits. */
static inline uint32_t sign_ex(uint32_t val, int len)
{
    if (val & (1UL << (len - 1)))   /* Has sign bit set on bit `len - 1` */
        return val | IMM_MIN(len);  /* Set top bits `32 - len` bits */
    else
        return val & UIMM_MAX(len); /* Not neg, clear all upper bits */
}


/*
 * Exported functions
 */

/* Lookup-Map Initialization */
void initialize_inst_maps(void);

/* Immediate Expression Generation and Evaluation */
immexpr_s *copy_immexpr(immexpr_s *old_expr);
imm_t eval_immexpr(immexpr_s *expr, memaddr_t loc, bool mask_bits);
immexpr_s *make_immexpr(imm_t offs, char *sym);

/* Instruction Production */
inst_s *copy_inst(inst_s *inst);
void free_inst(inst_s *inst);
memaddr_t store_i_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                            immexpr_s *expr, char *source_line);
memaddr_t store_r_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                            char *source_line);
memaddr_t store_r4_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                             reg_t rs3, char *source_line);

/* Instruction Memory */
void initialize_inst_memory(void);
void destroy_inst_memory(void);
void free_inst_memory(void);
inst_s *read_inst(memaddr_t addr, access_level_e access_level);
memaddr_t store_empty_inst(inst_s **inst);
memaddr_t store_inst(inst_s *inst);
void write_inst(memaddr_t addr, inst_s *inst, access_level_e access_level);

/* Breakpoint Management */
bool db_breakpoint_at_addr(memaddr_t addr);
bool inst_is_db_breakpoint(inst_s *inst);
inst_s *set_breakpoint(memaddr_t addr);

/* Instruction Encoding */
instword_t inst_encode(inst_s *inst);

/* Instruction Decoding */
inst_s *inst_decode(instword_t value);

/* Instruction Formatting and Printing */
char *inst_to_string(inst_s *inst);
char *inst_to_string_at_addr(memaddr_t addr);
void format_an_inst(sstream_s *ss, inst_s *inst, memaddr_t addr);
void format_immexpr(sstream_s *ss, inst_s *inst);
void print_inst(inst_s *inst);
void print_inst_at_addr(memaddr_t addr);

/* Utility - Validation */
bool validate_immexpr(immexpr_s *expr, memaddr_t addr);
void test_assembly(inst_s *inst);

#ifdef __cplusplus
}
#endif

#endif  // INST_H
