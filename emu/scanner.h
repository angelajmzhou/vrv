/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/*
 * Exported functions (besides yylex)
 */

#ifndef SCANNER_H
#define SCANNER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#include "inst.h"

void initialize_scanner(FILE *in_file);
void push_scanner(FILE *in_file);
void pop_scanner(void);
void set_error_token(int loc, int len);
void set_error_current_line(void);
char* erroneous_line(void);
void scanner_start_line(void);
char *source_line(void);
int yylex(void);

/*
 * Exported Variables
 */

/* The many possible context-relative types of a token */
typedef union {
    int32_t i;
    uint32_t u;
    float f;
    char *str;
    immexpr_s *expr;
    struct {
        char *str;
        int32_t i;
    } const_id;
} yylval_t;

#define YYSTYPE yylval_t

extern int line_no;     /* Line number in input file*/

#ifdef __cplusplus
}
#endif

#endif  // SCANNER_H
