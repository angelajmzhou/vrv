%{

/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "scanner.h" // <-- Must be first include for flex to see YYSTYPE

#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>

#include "csr.h"
#include "inst.h"
#include "map.h"
#include "op.h"
#include "param.h"
#include "parser.h"
#include "parser.tab.h"
#include "reg.h"
#include "sstream.h"
#include "symbol.h"
#include "utils.h"

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#define YY_NO_UNISTD_H

/*
 * Macros
 */

/* Utility macros */
#define TOKEN_FOUND                                         \
do {                                                        \
    if (current_line == NULL) {                             \
        current_line_no = line_no;                          \
        current_line = yytext;                              \
        full_line = yytext;                                 \
    }                                                       \
                                                            \
    yylloc.first_line = yylloc.last_line = current_line_no; \
    yylloc.first_column = yytext - full_line;               \
    yylloc.last_column = yylloc.first_column + yyleng;      \
} while (0)

#define LABEL_ERROR(error_msg)  \
do {                            \
    yytext[yyleng - 1] = ':';   \
    yyerror(error_msg);         \
} while (0)


/*
 * Exported Variables
 */

int line_no;    /* Line number in input file*/

/*
 * Local Variables
 */

// Track which line we are reading and where it began in the buffer
static int current_line_no = 0;

static char *current_line = NULL;   // Used for storing line in `inst_s`
static char *full_line = NULL;      // Used in `erroneous_line()`

static int error_tok_loc = -1;
static int error_tok_len = 0;

static int line_returned = 0;   // Returned current line yet?
static int eof_returned = 0;    // Return EOF token yet?


/*
 * Local functions
 */

static int check_keyword(char *id);
static int encode_mem_opr(char *mem_str);
static int csr_name_to_number(char *name);
static int register_name_to_number(char *name, int32_t *reg_no, bool *is_freg);
static int rm_name_to_number(char *name);
static char scan_escape(char **str);
static char *xstrdup_chop(char *str, int chop);

#undef yywrap

%}

%%

    /* Whitespace, skip */
[ \t] {
    if (current_line == NULL) {
        current_line_no = line_no;
        current_line = yytext;
        full_line = yytext;
    }
}

    /* Newline, terminate command (Y_NL) */
[\n] {
    return Y_NL;
}

    /* Carrage returns, ignore  */
[\r] { }

    /* Marker character inserted to allow scanner to return Y_EOF before
     * returning hard EOF. */
[\001] {
    return Y_EOF;
}

    /* Ignore comments */
"#".* { }

    /* Decimal integers, parse (Y_INT) */
[0-9]+ {
    TOKEN_FOUND;

    yylval.i = atoi(yytext);
    return Y_INT;
}

    /* Hex integers, parse (Y_INT) */
0x[0-9A-Fa-f]+ {
    TOKEN_FOUND;

    sscanf(yytext, "%x", (unsigned int *)&(yylval.i));
    return Y_INT;
}

    /* Floating point number, parse (Y_FP) */
(-[ \t]s*)?[0-9]+[\.\,\'][0-9]+(e)?(\+|\-)?[0-9]* {
    TOKEN_FOUND;

    if (*yytext == '-')
        yylval.f = (float)-atof(yytext + 1);
    else
        yylval.f = (float)atof(yytext);
    return Y_FP;
}

    /* "PC" used for prompt lookup and reserved to not be used as an ID */
[pP][cC] {
    TOKEN_FOUND;

    return Y_PC;
}

    /* Word-based tokens - check for certain classes of tokens in this order:
     * for register, rounding mode, fence memory operands mnemonic/directive, CSR,
     * Or finally interpret as a label (look for already-defined labels) */
[a-zA-Z_\.][a-zA-Z0-9_\.]*:? {
    TOKEN_FOUND;

    // If this is a line label, remove the colon to check for name collisions
    bool is_label = yytext[yyleng - 1] == ':';
    if (is_label) {
        yytext[yyleng - 1] = '\0';
    }

    // Check for register value as some ABI names are just 2-letters
    int32_t reg_no;
    bool is_freg;
    if (register_name_to_number(yytext, &reg_no, &is_freg)) {
        if (is_label)
            LABEL_ERROR("Cannot use register name as label");
        yylval.i = reg_no;
        return is_freg ? Y_FP_REG : Y_REG;
    }

    // Check for rounding mode operand
    int rm_no = rm_name_to_number(yytext);
    if (rm_no >= 0) {
        if (is_label)
            LABEL_ERROR("Cannot use rounding mode as label");
        yylval.i = rm_no;
        return Y_RM;
    }

    // Check for fence memory flags operand
    int mem_bits = encode_mem_opr(yytext);
    if (mem_bits > 0) {
        // Must make exception for `or` and `ori` instructions, parser will adjust
        if (is_label)
            LABEL_ERROR("Cannot use combination of \"iorw\" as label");
        if (mem_bits == 0x6 && !strcmp_ci(yytext, "or")) {
            yylval.i = Y_OR_OP;
            return Y_OR_OP;
        }
        if (mem_bits == 0xe && !strcmp_ci(yytext, "ori")) {
            yylval.i = Y_ORI_OP;
            return Y_ORI_OP;
        }
        yylval.i = mem_bits;
        return Y_MEM_FLAGS;
    }

    // Check for instruction menmonics and directives
    int token = check_keyword(yytext);
    label_s *l;
    if (token >= 0) {
        // Mnemonic or directive, return opcode value from table (Y_...)
        if (is_label)
            LABEL_ERROR("Cannot use instruction mnemonic or directive as label");
        yylval.i = token;
        current_line = yytext;
        return token;
    }

    // Check for CSR operand
    int csr_val = csr_name_to_number(yytext);
    if (csr_val >= 0) {
        if (is_label)
            LABEL_ERROR("Cannot use csr name as label");
        yylval.i = csr_val;
        return Y_CSR;
    }

    // If still undetermined, must be a label...
    // Already-defined, potential collision or defined int constant
    if ((l = label_is_defined(yytext)) != NULL) {
        if (is_label)
            LABEL_ERROR("Label is defined for second time");
        // Defined int constant, return value (Y_INT)
        if (l->const_flag) {
            yylval.const_id.i = l->addr;
            yylval.const_id.str = xstrdup(yytext);
            return Y_ID_CONST;
        }
    }

    // Otherwise, return label id string (Y_ID)
    if (yytext[0] == 'q')
        printf("%d\n", is_label);
    yylval.str = xstrdup(yytext);

    // Return colon to original string now that string has been copied
    if (is_label)
        yytext[yyleng - 1] = ':';

    return is_label ? Y_LABEL : Y_ID;
}

    /* Operator character, return itself */
[\*\/:()+-]|"="|">"|"<" {
    TOKEN_FOUND;

    return *yytext;
}

    /* Commas, skip */
"," {
    TOKEN_FOUND;

    return Y_COMMA;
}

    /* Double-quoted string, extract (Y_STR) */
    /* @TEMP: Original rule seemed eroneous, was: \"(([^""])|(\\\"))*\" */
\"([^\\\"]|\\.)*\" {
    TOKEN_FOUND;

    yylval.str = xstrdup_chop(yytext + 1, 1);
    return Y_STR;
}

    /* Single-quoted character, parse (Y_INT) */
    /* @TEMP: Original rule seemed eroneous, was: \'(([^''])|(\\[^'']))\' */
\'([^\\\']|\\.)*\' {
    TOKEN_FOUND;

    if (*(yytext + 1) == '\\') {
        char *escape = yytext + 2;
        yylval.i = (int)scan_escape(&escape);
    } else {
        yylval.i = (int)*(yytext + 1);
    }

    return Y_INT;
}

    /* All other characters, error */
. {
    TOKEN_FOUND;

    yyerror("Unknown character");
}

%%

void initialize_scanner(FILE *in_file)
{
    if (yyin != in_file) {
        push_scanner(in_file);
    }
    yyin = in_file;

#ifdef FLEX_SCANNER
    yyrestart(in_file);
#define YY_FLEX_VERSION (YY_FLEX_MAJOR_VERSION * 1000 + YY_FLEX_MINOR_VERSION * 100 + YY_FLEX_SUBMINOR_VERSION)
#if YY_FLEX_VERSION >= 2533
    /* flex 2.5.33 flipped the polarity of this flag (sigh) */
    yy_init = 0;
#else
    yy_init = 1;
#endif
#endif

    line_no = 1;
    current_line = NULL;
    line_returned = 0;
    eof_returned = 0;

    error_tok_loc = -1;
    error_tok_len = 0;
}

void push_scanner(FILE *in_file)
{
    YY_BUFFER_STATE buf = yy_create_buffer(in_file, YY_BUF_SIZE);
    yypush_buffer_state(buf);
}

void pop_scanner(void)
{
    yypop_buffer_state();
}

void scanner_start_line(void)
{
    current_line = NULL;
    line_returned = 0;

    error_tok_loc = -1;
    error_tok_len = 0;
}


/* This is a work-around for a bug in flex v 2.5.31 (but not earlier or later
 * versions such as 2.5.4) that left this symbol undefined. */
#ifndef yytext_ptr
#define yytext_ptr yytext
#endif


/* Use yywrap to insert a marker character, which causes the scanner to return
 * Y_EOF, before return a hard EOF.  This wouldn't be necessary, except that
 * bison does not allow the parser to use EOF (= 0) as a non-terminal */
int yywrap(void)
{
    if (eof_returned)
        return 1;
    else
    {
        unput('\001');
        eof_returned = 1;
#ifdef FLEX_SCANNER
        yy_did_buffer_switch_on_eof = 1;
#endif
        return 0;
    }
}

/* A backslash has just been read, return the character designated by *STR. */
static char scan_escape(char **str)
{
    char first = **str;
    *str += 1;
    switch (first)
    {
        // Standard escape character
        case 'a':   return '\a';
        case 'b':   return '\b';
        case 'f':   return '\f';
        case 'n':   return '\n';
        case 'r':   return '\r';
        case 't':   return '\t';
        case '\\':  return '\\';
        case '"':   return '"';
        case '\'':  return '\'';

        // Hex character, parse
        case 'x':
        case 'X':
        {
            char c1 = **str, c2 = *(*str + 1);
            int b = 0;

            if      ('0' <= c1 && c1 <= '9') b = c1 - '0';
            else if ('A' <= c1 && c1 <= 'F') b = c1 - 'A' + 10;
            else if ('a' <= c1 && c1 <= 'f') b = c1 - 'a' + 10;
            else yyerror("Bad character in \\X construct in string");

            b <<= 4;
            if      ('0' <= c2 && c2 <= '9') b += c2 - '0';
            else if ('A' <= c2 && c2 <= 'F') b += c2 - 'A' + 10;
            else if ('a' <= c2 && c2 <= 'f') b += c2 - 'a' + 10;
            else yyerror("Bad character in \\X construct in string");

            *str += 2;
            return (char)b;
        }

        // Unrecognized escape character, error
        default:
        {
            char message[] = "Bad character \\X";
            message[strlen(message) - 1] = first;
            yyerror(message);
            return '\0';
        }
    }
}


/* Return a freshly-allocated copy of STRING with the last CHOP characters
 * removed. */
static char *xstrdup_chop(char *str, int chop)
{
    int new_len = strlen(str) - chop;
    char *new_str = (char *)xmalloc(new_len + 1), *n;

    // Interpret escaped characters
    // @HELP: Not sure why this block interprets octals while `scan_escape` does not
    for (n = new_str; *str != '\0' && new_len > 0; new_len -= 1) {
        if (*str == '\\') {
            switch (*(str + 1))
            {
                case 'n':
                {
                    *n ++ = '\n';
                    str += 2;
                    new_len -= 1;
                    continue;
                }
                case 't':
                {
                    *n ++ = '\t';
                    str += 2;
                    new_len -= 1;
                    continue;
                }
                case '"':
                {
                    *n ++ = '"';
                    str += 2;
                    new_len -= 1;
                    continue;
                }
                case '0':    /* \nnn */
                case '1':
                case '2':
                case '3':
                {
                    char c2 = *(str + 2), c3 = *(str + 3);
                    int b = (*(str + 1) - '0') << 3;

                    if ('0' <= c2 && c2 <= '7')
                        b += (c2 - '0') << 3;
                    else
                        yyerror ("Bad character in \\ooo construct in string");

                    if ('0' <= c3 && c3 <= '7')
                        b += c3 - '0';
                    else
                        yyerror ("Bad character in \\ooo construct in string");

                    *n ++ = (char) b;
                    str += 4;
                    new_len -= 3;
                    continue;
                }
                case 'X':
                {
                    char c2 = *(str + 2), c3 = *(str + 3);
                    int b = 0;

                    if ('0' <= c2 && c2 <= '9')
                        b = c2 - '0';
                    else if ('A' <= c2 && c2 <= 'F')
                        b = c2 - 'A' + 10;
                    else
                        yyerror ("Bad character in \\X construct in string");

                    b <<= 4;
                    if ('0' <= c3 && c3 <= '9')
                        b += c3 - '0';
                    else if ('A' <= c3 && c3 <= 'F')
                        b += c3 - 'A' + 10;
                    else
                        yyerror ("Bad character in \\X construct in string");

                    *n ++ = (char) b;
                    str += 4;
                    new_len -= 3;
                    continue;
                }
                default:
                {
                    *n ++ = *str ++;
                    continue;
                }
            }
        }
        else
            *n ++ = *str ++;
    }

    *n = '\0';
    return new_str;
}


/* On a parse error, write out the current line and print a caret (^) below the
 * point at which the error occured.  Also, reset the input stream to the
 * begining of the next line. */
char* erroneous_line(void)
{
    int prefix_length = yytext - full_line;
    int c;
    sstream_s ss;

    // Set foucsed error token before lexing more tokens
    int curr_error_tok_loc = error_tok_loc;
    int curr_error_tok_len = error_tok_len;
    if (curr_error_tok_loc < 0) {
        curr_error_tok_loc = yylloc.first_column;
        curr_error_tok_len = yylloc.last_column - yylloc.first_column;
    }

    ss_init (&ss);

    if (full_line == NULL)
        return ss_to_string (&ss);
    // Print part of line that has been consumed.
    if (prefix_length >= 0) {
        // yytext and full_line point to same line
        c = *(full_line + prefix_length);
        *(full_line + prefix_length) = '\0';
        ss_printf(&ss, "%s", full_line);
        *(full_line + prefix_length) = (char)c;
        if (c != '\n')
            ss_printf(&ss, "%s", yytext);
    } else {
        // yytext and full_line point to different line
        ss_printf(&ss, "%s", full_line);
        prefix_length = strlen(full_line);
    }

    // Flush the rest of the line (not consumed) from lex input.
    if (*yytext != '\n') {
#ifdef __cplusplus
        while ((c = yyinput()) != '\n' && c != EOF && c != 1)
#else
            while ((c = input()) != '\n' && c != EOF && c != 1)
#endif
            {
                ss_printf(&ss, "%c", c);
            }
        if (c == '\n')
            unput ('\n');

        current_line = NULL;
        full_line = NULL;
    }

    // Print marker to point at which consumption stopped.
    ss_printf(&ss, "\n    ");
    for (int i = 0; i < curr_error_tok_loc; ++i)
        ss_printf(&ss, " ");
    ss_printf(&ss, "^");
    for (int i = 1; i < curr_error_tok_len; ++i)
        ss_printf(&ss, "~");
    ss_printf(&ss, "\n");

    return ss_to_string(&ss);
}

static int check_keyword(char *id)
{
    op_s *entry = SEARCH_OP_NAME(id);
    if (entry == NULL)
        return -1;
    return entry->i_opcode;
}

static int csr_name_to_number(char *name)
{
    csr_name_s *entry = SEARCH_CSR_NAME(name);
    if (entry == NULL)
        return -1;
    return entry->encoding;
}

static int register_name_to_number(char *name, int32_t *reg, bool *freg)
{
    size_t len = strlen(name);
    if (len < 2)
        return 0;

    char c1 = *name, c2 = *(name + 1);

    // Check for `[xf]\d+` pattern
    bool has_reg_no = (c2 >= '1' && c2 <= '9') || (c2 == '0' && len == 2);
    bool is_freg = c1 == 'f' || c2 == 'F';
    if (has_reg_no && (is_freg || c1 == 'x' || c1 == 'X')) {
        // Parse integer
        int32_t reg_no = atoi(name + 1);
        // Check integer bounds, throw `yyerror` if out of bounds
        if (reg_no >= R_LENGTH) {
            if (is_freg)
                yyerror("FP register number out of range");
            else
                yyerror("Register number out of range");
            // Return 0 so that the rule understands this as `Y_REG`
            //   despite the error, allowing the parser to find other errors.
            reg_no = 0;
        }
        // Return parsed and validated register number
        *freg = is_freg;
        *reg = reg_no;
        return 1;
    }

    // Search for ABI register name
    name_val_s *entry = SEARCH_GPR_NAME(name);
    if (entry) {
        *freg = false;
        *reg = entry->val;
        return 1;
    }
    entry = SEARCH_FPR_NAME(name);
    if (entry) {
        *freg = true;
        *reg = entry->val;
        return 1;
    }

    return 0;
}

static int rm_name_to_number(char *name)
{
    // Linear search through map
    for (size_t i = 0; i < rm_name_map_size; ++i) {
        if (!strcmp_ci(name, rm_name_map[i].name))
            return rm_name_map[i].val;
    }
    return -1;
}

static int encode_mem_opr(char *mem_str)
{
    int out = 0;

    if (strlen(mem_str) > 4)
        return -1;

    // Set individual bits based on char, but fail on any repeat occurrence
    while (*mem_str != '\0') {
        switch (*mem_str) {
            case 'i':
            case 'I':
                if (out & 8)
                    return -1;
                out |= 8;
                break;
            case 'o':
            case 'O':
                if (out & 4)
                    return -1;
                out |= 4;
                break;
            case 'r':
            case 'R':
                if (out & 2)
                    return -1;
                out |= 2;
                break;
            case 'w':
            case 'W':
                if (out & 1)
                    return -1;
                out |= 1;
                break;
            default:
                return -1;
        }
        ++mem_str;
    }

    return out > 0 ? out : -1;
}


/* Exactly once, return the current source line, as a printable string with a
 * line number.  Subsequent calls receive NULL instead of the line. */
char *source_line(void)
{
    if (line_returned)
        return NULL;
    else if (current_line == NULL)  /* Error on line */
        return NULL;
    else {
        char *eol1, c1;
        char *null1 = NULL;
        char *r;

        // Find end of line:
        for (eol1 = current_line; *eol1 != '\0' && *eol1 != '\n'; )
            eol1 += 1;

#ifdef FLEX_SCANNER
        /* Ran into null byte, inserted by yylex. If necessary, look further
           for newline. (This only works for scanners produced by flex. Other
           versions of lex need similar code, or source code lines will end
           early. */
        if (*eol1 == '\0' && yy_hold_char != '\n') {
            null1 = eol1;
            *eol1 = yy_hold_char;
            for ( ; *eol1 != '\0' && *eol1 != '\n'; )
                eol1 += 1;
        }
#endif

        /* Save end-of-line character and null terminate string so it can
           be printed. */
        c1 = *eol1;
        *eol1 = '\0';

        r = (char *)xmalloc(eol1 - current_line + 10);
        sprintf(r, "%d: %s", current_line_no, current_line);

        /* Restore end-of-line character and, if necessary, yylex's null byte. */
        *eol1 = c1;
        if (null1 != NULL) {
            *null1 = '\0';
        }
        line_returned = 1;
        return ((char *)r);
    }
}


void set_error_token(int loc, int len)
{
    error_tok_loc = loc;
    error_tok_len = len;
}

void set_error_current_line(void)
{
    error_tok_loc = current_line - full_line;
    error_tok_len = yylloc.last_column - error_tok_loc;
}
