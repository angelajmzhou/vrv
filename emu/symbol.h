/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef SYMBOL_H
#define SYMBOL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "inst.h"
#include "memloader.h"
#include "memseg.h"

/*
 * Structs
 */

/* Linked-list of insts which reference a particular as-yet undefined label */
typedef struct label_use {
    inst_s *inst;               // When NULL, use `expr` and interpret as data
    immexpr_s *data_expr;
    memaddr_t addr;
    struct label_use *next;
    bool free_inst:1;           // true => free `inst` after resolution
} label_use_s;

/* Symbol table information on a label. */
typedef struct label {
    char *name;                 // Name of label
    memaddr_t addr;             // Address of label or 0 if not yet defined
    struct label *next;         // Hash table link
    struct label *next_local;   // Link in list of local labels
    label_use_s *uses;          // List of instructions that reference
    bool global_flag:1;         // Non-zero => declared global
    bool gp_flag:1;             // Non-zero => referenced off gp
    bool const_flag:1;          // Non-zero => constant value (in addr)
} label_s;


/*
 * Inline functions
 */

static inline bool symbol_is_defined(label_s *sym)
{
    return sym->gp_flag || sym->const_flag || sym->addr != 0;
}

static inline bool expr_is_gp_rel(immexpr_s *expr)
{
    return expr->symbol
        && expr->symbol->gp_flag
        && (expr->base_reg == 0 || expr->base_reg == 3);
}

static inline bool expr_is_pc_rel(immexpr_s *expr)
{
    return expr->pc_relative && expr->symbol && !expr->symbol->const_flag;
}


/*
 * Exported functions
 */

/* Initialization + Destruction */
void destroy_symbol_table(void);

/* Management */
void flush_local_labels(void);
void destroy_lookup_list(label_s *label);

/* Lookup */
memaddr_t find_symbol_address(char *name);
label_s *label_is_defined(char *name);
bool lookup_or_make_label(char *name, label_s **label);
label_s *lookup_all_matching_defined_labels(char *name);

/* Recording */
label_s *make_label_global(char *name);
void record_data_uses_symbol(immexpr_s *expr, memaddr_t location, label_s *sym);
void record_inst_uses_symbol(inst_s *inst, memaddr_t inst_addr, label_s *sym);
label_s *record_const_label(char *name, memaddr_t val, bool resolve_uses);
label_s *record_label(char *name, memaddr_t address, bool resolve_uses);

/* Resolving */
void data_resolve_label(label_s *sym, immexpr_s *expr, memaddr_t addr);
void inst_resolve_label(label_s *sym, inst_s *inst);
void resolve_label_uses(label_s *sym);

/* Printing */
void print_symbols(bool print_retired);
char *undefined_symbol_string(void);

#ifdef __cplusplus
}
#endif

#endif  // SYMBOL_H
