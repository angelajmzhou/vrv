/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "frontend.h"
#include "tty.h"


/* ========================================================================== *
 * Bitfield Macros
 * ========================================================================== */

#define TTY_WBIT    0x1
#define TTY_RBIT    0x2


/* ========================================================================== *
 * TTY Registers and State
 * ========================================================================== */

static memword_t read_reg;
static memword_t writ_reg;
static memword_t ctrl_reg;
static memword_t stat_reg;

/* Necessary to prevent indefinite interrupts when ready to transmit.
 * Set initialy and after reads to WRIT, cleared after a write to WRIT. */
static bool writ_irq_lowered;


/* ========================================================================== *
 * Interrupt Logic
 * ========================================================================== */

static void tty_update_irq(void)
{
    /* Raise IRQ bit when either READ or WRIT has a new character.
     * However, prevent WRIT-caused IRQs when explicitly lowered
     *   (from software reads to WRIT register). */
    /* @NOTE: Will need to rethink this when implementing latency */
    memword_t irq_mask = TTY_RBIT | (writ_irq_lowered ? 0 : TTY_WBIT);
    if (ctrl_reg & stat_reg & irq_mask)
        C[CSR_MIP] |= INT_M_EXTERNAL_BIT;
    else
        C[CSR_MIP] &= ~INT_M_EXTERNAL_BIT;
}


/* ========================================================================== *
 * Management
 * ========================================================================== */

void initialize_tty(void)
{
    /* Clear all data, except set STAT.WBIT as device can receive transmissions */
    writ_reg = 0;
    read_reg = 0;
    ctrl_reg = 0;
    stat_reg = TTY_WBIT;
    writ_irq_lowered = true;
}


/* ========================================================================== *
 * Access
 * ========================================================================== */

memword_t tty_read(memaddr_t offset, int num_bytes, access_level_e al)
{
    if (num_bytes != BP_WORD) {
        fe->write_log(1,
            "TTY warning: sub-word read from register at offset: 0x%02x\n",
            offset);
        return 0;
    }

    if (al < EMU_ACCESS) {
        /* Accessing registers from execution */
        memword_t val = 0;
        switch (offset) {
            case TTY_WRIT_OFFSET:   /* Forcibly lowers WRIT IRQ */
                val = writ_reg;
                writ_reg = 0;       /* Reads to data reg clears value */
                writ_irq_lowered = true;
                tty_update_irq();
                break;

            case TTY_READ_OFFSET:   /* Clears STAT.RBIT */
                val = read_reg;
                read_reg = 0;       /* Reads to data reg clears value */
                stat_reg &= ~TTY_RBIT;
                tty_update_irq();
                break;

            case TTY_STAT_OFFSET:   /* No side-effects */
                val = stat_reg;
                break;

            default:
                fe->write_log(1,
                    "TTY warning: invalid write to register at offset: 0x%02x\n",
                    offset);
        }
        return val;

    } else {
        /* Accessing registers from frontend, unconditional and no side-effects */
        switch (offset) {
            case TTY_WRIT_OFFSET:
                return writ_reg;
            case TTY_READ_OFFSET:
                return read_reg;
            case TTY_CTRL_OFFSET:
                return ctrl_reg;
            case TTY_STAT_OFFSET:
                return stat_reg;
            default:
                fe->write_log(1,
                    "TTY warning: invalid write to register at offset: 0x%02x\n",
                    offset);
                return 0;
        }
    }
}

void tty_write(memaddr_t offset, memword_t data, int num_bytes, access_level_e al)
{
    if (num_bytes != BP_WORD) {
        fe->write_log(1,
            "TTY warning: sub-word write to register at offset: 0x%02x\n",
            offset);
        return;
    }

    if (al < EMU_ACCESS) {
        switch (offset) {
            /* Accessing registers from execution */
            case TTY_WRIT_OFFSET:
                /* Lowers STAT.WBIT but allows next WRIT IRQ */
                writ_reg = data & 0xff;
                stat_reg &= ~TTY_WBIT;
                writ_irq_lowered = false;
                tty_update_irq();
                break;

            case TTY_CTRL_OFFSET:
                ctrl_reg = data & (TTY_RBIT | TTY_WBIT);
                tty_update_irq();
                break;

            default:
                fe->write_log(1,
                    "TTY warning: invalid read from register at offset: 0x%02x\n",
                    offset);
        }

    } else {
        /* Accessing registers from frontend, unconditional and no side-effects */
        switch (offset) {
            case TTY_WRIT_OFFSET:
                writ_reg = data;
                break;
            case TTY_READ_OFFSET:
                read_reg = data;
                break;
            case TTY_CTRL_OFFSET:
                ctrl_reg = data;
                break;
            case TTY_STAT_OFFSET:
                stat_reg = data;
                break;
            default:
                fe->write_log(1,
                    "TTY warning: invalid read from register at offset: 0x%02x\n",
                    offset);
        }
    }
}


/* ========================================================================== *
 * Polling
 * ========================================================================== */

void check_tty_device(void)
{
    /* Check for characters from physical tty */
    if (!(stat_reg & TTY_RBIT) && fe->console_input_available()) {
        /* STAT.RBIT not set and input available, so read in new character */
        char c = fe->get_console_char();
        read_reg = c;
        stat_reg |= TTY_RBIT;
        tty_update_irq();
    }

    /* Checks for transmissions from WRIT */
    if (!(stat_reg & TTY_WBIT)) {
        /* STAT.WBIT lowered as side-effect to write, so intake the character */
        fe->put_console_char(writ_reg);
        stat_reg |= TTY_WBIT;
        tty_update_irq();
    }
}
