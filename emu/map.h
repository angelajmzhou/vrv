/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef MAP_H
#define MAP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include "csr.h"
#include "op.h"
#include "run.h"


/*
 * Typedefs
 */

typedef int (*comp_f)(const void *, const void *);


/*
 * Structs
 */

typedef struct name_val {
    char *name;
    int val;
} name_val_s;


/*
 * Exported functions
 */

/* Initialization */
void initialize_maps(void);


/*
 * Exported maps
 */

// Map from ABI register names to their numbers, used by `scanner.l` when parsing
extern name_val_s   gpr_name_map[];
extern size_t       gpr_name_map_size;

extern name_val_s   fpr_name_map[];
extern size_t       fpr_name_map_size;

// Map from FP rounding modes to their encodings, used by `scanner.l` when parsing
extern name_val_s   rm_name_map[];
extern size_t       rm_name_map_size;

// Map from CSR encodings to their names, used by `inst.c` when printing
extern csr_encoding_s csr_encoding_map[];
extern size_t       csr_encoding_map_size;

// Map from CSR names to their encodings, used by `scanner.l` when parsing
extern csr_name_s   csr_name_map[];
extern size_t       csr_name_map_size;

// Map from actual opcodes to `op_s` entry, used by `inst.c` when decoding
extern op_s         a_opcode_map[];
extern size_t       a_opcode_map_size;

// Map from internal opcodes to `op_s` entry, used by `inst.c` when encoding and printing
extern op_s         i_opcode_map[];
extern size_t       i_opcode_map_size;

// Map from instruction mnemonics to `op_s` entry, used by `scanner.l` when parsing
extern op_s         op_name_map[];
extern size_t       op_name_map_size;


/* -------------------------------------------------------------------------- *
 * Compare - `name_val_s`
 * -------------------------------------------------------------------------- */

static inline int __compare_name(const name_val_s *a, const name_val_s *b)
{
    return strcmp_ci(a->name, b->name);
}

static inline int __compare_val(const name_val_s *a, const name_val_s *b)
{
    return a->val - b->val;
}


/* -------------------------------------------------------------------------- *
 * Lookup - `name_val_s`
 * -------------------------------------------------------------------------- */

static inline name_val_s *__search_name_val(name_val_s *k, name_val_s *m, size_t s, comp_f f)
{
    return (name_val_s *)bsearch(k, m, s, sizeof(name_val_s), f);
}

#define SEARCH_NAME(name, map, map_size)        \
({                                              \
    name_val_s __k = { name, 0 };               \
    name_val_s *__d = __search_name_val(&__k,   \
        map, map_size, (comp_f)__compare_name); \
    __d;                                        \
})

#define SEARCH_VAL(val, map, map_size)         \
({                                             \
    name_val_s __k = { "", val };              \
    name_val_s *__d = __search_name_val(&__k,  \
        map, map_size, (comp_f)__compare_val); \
    __d;                                       \
})


/* -------------------------------------------------------------------------- *
 * Compare - `csr_s`
 * -------------------------------------------------------------------------- */

static inline int __compare_csr_encoding(const csr_encoding_s *a,
                                         const csr_encoding_s *b)
{
    return (a->encoding - b->encoding);
}

static inline int __compare_csr_name(const csr_name_s *a, const csr_name_s *b)
{
    return strcmp_ci(a->name, b->name);
}


/* -------------------------------------------------------------------------- *
 * Compare - `op_s`
 * -------------------------------------------------------------------------- */

static inline int __compare_a_opcode(const op_s *a, const op_s *b)
{
    // Must compapre directly as these are unsigned
    return a->a_opcode > b->a_opcode ? 1 : (
        a->a_opcode < b->a_opcode ? -1 : 0
    );
}

static inline int __compare_i_opcode(const op_s *a, const op_s *b)
{
    return a->i_opcode - b->i_opcode;
}

static inline int __compare_op_name(const op_s *a, const op_s *b)
{
    return strcmp_ci(a->name, b->name);
}


/* -------------------------------------------------------------------------- *
 * Lookup - `op_s`
 * -------------------------------------------------------------------------- */

static inline op_s *__search_op(op_s *k, op_s *m, size_t s, comp_f f)
{
    return (op_s *)bsearch(k, m, s, sizeof(op_s), f);
}


/* ========================================================================== *
 * Map Search Macros
 * ========================================================================== */

#define SEARCH_GPR_NAME(k)  SEARCH_NAME(k, gpr_name_map, gpr_name_map_size);
#define SEARCH_FPR_NAME(k)  SEARCH_NAME(k, fpr_name_map, fpr_name_map_size);
#define SEARCH_RM_NAME(k)   SEARCH_NAME(k, rm_name_map, rm_name_map_size);
#define SEARCH_CSR_ENCODING(csr_encoding)                          \
({                                                                 \
    csr_encoding_s __k = { "", 0, csr_encoding, 0, 0 };            \
    csr_encoding_s *__d = bsearch(&__k, csr_encoding_map,          \
                                  csr_name_map_size,               \
                                  sizeof(csr_encoding_s),          \
                                  (comp_f)__compare_csr_encoding); \
    __d;                                                           \
})
#define SEARCH_CSR_NAME(csr_name)                          \
({                                                         \
    csr_name_s __k = { csr_name, 0, };                     \
    csr_name_s *__d = bsearch(&__k, csr_name_map,          \
                              csr_name_map_size,           \
                              sizeof(csr_name_s),          \
                              (comp_f)__compare_csr_name); \
    __d;                                                   \
})
#define SEARCH_A_OPCODE(a_opcode)                       \
({                                                      \
    op_s __k = { "", 0, 0, a_opcode };                  \
    op_s *__d = __search_op(&__k, a_opcode_map,         \
        a_opcode_map_size, (comp_f)__compare_a_opcode); \
    __d;                                                \
})
#define SEARCH_I_OPCODE(i_opcode)                       \
({                                                      \
    op_s __k = { "", i_opcode, 0, 0 };                  \
    op_s *__d = __search_op(&__k, i_opcode_map,         \
        i_opcode_map_size, (comp_f)__compare_i_opcode); \
    __d;                                                \
})
#define SEARCH_OP_NAME(op_name)                       \
({                                                    \
    op_s __k = { op_name, 0, 0, 0 };                  \
    op_s *__d = __search_op(&__k, op_name_map,        \
        op_name_map_size, (comp_f)__compare_op_name); \
    __d;                                              \
})

#ifdef __cplusplus
}
#endif

#endif  // MAP_H
