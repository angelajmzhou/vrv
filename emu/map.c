/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "csr.h"
#include "inst.h"
#include "map.h"
#include "op.h"
#include "parser.tab.h"
#include "reg.h"

/*
 * Map definitions
 */

name_val_s gpr_name_map[] = {
#define GPR_MAP(name, val) \
    { name, val },
#include "gpr_map.h"
#undef GPR_MAP
};
size_t gpr_name_map_size = ARRAY_SIZE(gpr_name_map);

name_val_s fpr_name_map[] = {
#define FPR_MAP(name, val) \
    { name, val },
#include "fpr_map.h"
#undef FPR_MAP
};
size_t fpr_name_map_size = ARRAY_SIZE(fpr_name_map);


name_val_s rm_name_map[] = {
    { "rne",    0 },
    { "rtz",    1 },
    { "rdn",    2 },
    { "rup",    3 },
    { "rmm",    4 },
    { "dyn",    7 },
};
size_t rm_name_map_size = ARRAY_SIZE(rm_name_map);


csr_encoding_s csr_encoding_map[] = {
#define CSR_MAP(name, index, number, mask, shift) \
    {name, index, number, mask, shift},
#include "csr_map.h"
#undef CSR_MAP
};
size_t csr_encoding_map_size = ARRAY_SIZE(csr_encoding_map);


csr_name_s csr_name_map[] = {
#define CSR_MAP(name, index, number, mask, shift) \
    {name, number},
#include "csr_map.h"
#undef CSR_MAP
};
size_t csr_name_map_size = ARRAY_SIZE(csr_name_map);


op_s a_opcode_map[] = {
#include "op_map.h"
};
size_t a_opcode_map_size = ARRAY_SIZE(a_opcode_map);


op_s op_name_map[] = {
#include "op_map.h"
};
size_t op_name_map_size = ARRAY_SIZE(op_name_map);


op_s i_opcode_map[] = {
#include "op_map.h"
};
size_t i_opcode_map_size = ARRAY_SIZE(i_opcode_map);


/* ========================================================================== *
 * Initialization
 * ========================================================================== */

void initialize_maps(void)
{
    qsort(gpr_name_map,      gpr_name_map_size,      sizeof(name_val_s),  (comp_f)__compare_name);
    qsort(fpr_name_map,      fpr_name_map_size,      sizeof(name_val_s),  (comp_f)__compare_name);
    qsort(rm_name_map,       rm_name_map_size,       sizeof(name_val_s),  (comp_f)__compare_name);
    qsort(csr_encoding_map,  csr_encoding_map_size,  sizeof(csr_encoding_s),    (comp_f)__compare_csr_encoding);
    qsort(csr_name_map,      csr_name_map_size,      sizeof(csr_name_s),        (comp_f)__compare_csr_name);
    qsort(op_name_map,       op_name_map_size,       sizeof(op_s),        (comp_f)__compare_op_name);
    qsort(i_opcode_map,      i_opcode_map_size,      sizeof(op_s),        (comp_f)__compare_i_opcode);
    qsort(a_opcode_map,      a_opcode_map_size,      sizeof(op_s),        (comp_f)__compare_a_opcode);
}
