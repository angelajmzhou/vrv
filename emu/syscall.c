/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>

#include "csr.h"
#include "frontend.h"
#include "memaccess.h"
#include "memloader.h"
#include "param.h"
#include "syscall.h"
#include "utils.h"


/*
 * OS-dependnet macros
 */

#ifdef _WIN32
#define OPEN_OS   _open
#define CLOSE_OS  _close
#define READ_OS   _read
#define WRITE_OS  _write
#else
#define OPEN_OS  open
#define CLOSE_OS close
#define READ_OS  read
#define WRITE_OS write
#endif


/*
 * Local functions
 */

static char *get_memory_string(memaddr_t addr);
static bool get_memory_string_n(memaddr_t addr, char **str, size_t n);
static void store_memory_string_n(memaddr_t addr, char *str, size_t n);


#ifdef _WIN32
/* ========================================================================== *
 * Windows Parameter Handler
 * ========================================================================== */

/* Windows has a handler that is invoked when an invalid argument is passed to
 * a system call.
 * https://msdn.microsoft.com/en-us/library/a9yf33zb(v=vs.110).aspx
 *
 * All good, except that the handler tries to invoke Watson and then kill VRV
 * with an exception.
 *
 * Override the handler to just report an error.
 */

#include <stdio.h>
 * with an exception.
 *
 * Override the handler to just report an error.
 */

#include <stdio.h>
#include <stdlib.h>
#include <crtdbg.h>

void myInvalidParameterHandler(const wchar_t *expression,
                               const wchar_t *function,
                               const wchar_t *file,
                               unsigned int line, uintptr_t pReserved)
{
    if (function != NULL) {
        fe->run_error("Bad parameter to system call: %s\n", function);
    } else {
        fe->run_error("Bad parameter to system call\n");
    }
}

static _invalid_parameter_handler oldHandler;

void windowsParameterHandlingControl(int flag)
{
    static _invalid_parameter_handler oldHandler;
    static _invalid_parameter_handler newHandler = myInvalidParameterHandler;

    if (flag == 0) {
        oldHandler = _set_invalid_parameter_handler(newHandler);
        _CrtSetReportMode(_CRT_ASSERT, 0);  // Disable the message box for assertions.
    } else {
        newHandler = _set_invalid_parameter_handler(oldHandler);
        _CrtSetReportMode(_CRT_ASSERT, 1);  // Enable the message box for assertions.
    }
}
#endif


/* ========================================================================== *
 * Syscall String Helpers
 * ========================================================================== */

#define BUFF_SIZE 1024
static char buff[BUFF_SIZE];

static char *get_memory_string(memaddr_t addr)
{
    char *cp = buff;
    do {
        *cp = read_byte(addr++, ctx.current_access_level);
    } while (*(cp++));
    return buff;
}

/* Returns true when string was allocated instead of using static buffer. */
static bool get_memory_string_n(memaddr_t addr, char **str, size_t n)
{
    bool alloced_str = R[REG_A2] > BUFF_SIZE;
    *str = alloced_str ? (char *)xmalloc(R[REG_A2]) : buff;
    for (size_t i = 0; i < n; ++i) {
        (*str)[i] = read_byte(addr + i, ctx.current_access_level);
    }
    return alloced_str;
}

static void store_memory_string_n(memaddr_t addr, char *str, size_t n)
{
    for (size_t i = 0; i < n; ++i)
        write_byte(addr++, *(str++), ctx.current_access_level);
}


/* ========================================================================== *
 * Syscall Handler
 * ========================================================================== */

/* Decide which syscall to execute or simulate.
 * Returns false to signal `exit` syscall. */
sysres_e do_syscall(void)
{
#ifdef _WIN32
    // Set windows parameter handler
    windowsParameterHandlingControl(0);
#endif

    switch (R[REG_SYSCODE]) {

        /* Console print syscalls */
        case PRINT_DEC_SYSCALL:
            fe->write_output("%d", R[REG_A0]);
            break;
        case PRINT_HEX_SYSCALL:
            fe->write_output("0x%08x", R[REG_A0]);
            break;
        case PRINT_FLOAT_SYSCALL:
            fe->write_output("%.8f", FR[REG_FA0]);
            break;
        case PRINT_CHAR_SYSCALL:
            fe->write_output("%c", (char)R[REG_A0]);
            break;
        case PRINT_STRING_SYSCALL:
            {
                memaddr_t addr = UR[REG_A0];
                char c;
                while ((c = read_byte(addr++, ctx.current_access_level)))
                    fe->write_output("%c", c);
            }
            break;

        /* Console read syscalls */
        case READ_DEC_SYSCALL:
            if (!fe->read_input(buff, BUFF_SIZE))
                return SYSRES_WAIT_FOR_INPUT;
            R[REG_RES] = atol(buff);
            break;
        case READ_HEX_SYSCALL:
            if (!fe->read_input(buff, BUFF_SIZE))
                return SYSRES_WAIT_FOR_INPUT;
            R[REG_RES] = strtol(buff, NULL, 16);
            break;
        case READ_FLOAT_SYSCALL:
            if (!fe->read_input(buff, BUFF_SIZE))
                return SYSRES_WAIT_FOR_INPUT;
            FR[FREG_RES] = (float)atof(buff);
            break;
        case READ_CHAR_SYSCALL:
            if (!fe->read_input(buff, 2))
                return SYSRES_WAIT_FOR_INPUT;
            R[REG_RES] = (uregword_t)buff[0];
            break;
        case READ_STRING_SYSCALL:
        {
            bool alloced_str = UR[REG_A1] > BUFF_SIZE;
            char *str = alloced_str ? (char *)xmalloc(UR[REG_A1]) : buff;
            if (!fe->read_input(str, R[REG_A1])) {
                if (alloced_str)
                    free(str);
                return SYSRES_WAIT_FOR_INPUT;
            }

            memaddr_t addr = UR[REG_A0];
            char *cp = str;
            do {
                write_byte(addr++, *cp, ctx.current_access_level);
            } while (*(cp++));

            if (alloced_str)
                free(str);
            break;
        }

        /* Management syscalls */
        case EXIT_SYSCALL:
            ctx.vrv_exit_code = R[REG_A0];
            return SYSRES_EXIT;
        case SBRK_SYSCALL:
            R[REG_RES] = M[DATA_SEG].upper_bound;
            if (memory_sbrk(DATA_SEG, R[REG_A0]) != R[REG_A0]) {
                fe->run_error("sbrk: could not adjust data segment break by %d bytes\n", R[REG_A0]);
            }
            break;

        /* File IO syscalls */
        case OPEN_SYSCALL:
            R[REG_RES] = OPEN_OS(get_memory_string(UR[REG_A0]), R[REG_A1], R[REG_A2]);
            break;
        case CLOSE_SYSCALL:
            R[REG_RES] = CLOSE_OS(R[REG_A0]);
            break;
        case READ_SYSCALL:
        {
            bool alloced_str = UR[REG_A2] > BUFF_SIZE;
            char *str = alloced_str ? (char *)xmalloc(UR[REG_A2]) : buff;
            R[REG_RES] = READ_OS(R[REG_A0], str, UR[REG_A2]);
            store_memory_string_n(UR[REG_A1], str, UR[REG_A2]);
            if (alloced_str)
                free(str);
            break;
        }
        case WRITE_SYSCALL:
        {
            char *str;
            bool alloced_str = get_memory_string_n(UR[REG_A1], &str, UR[REG_A2]);
            R[REG_RES] = WRITE_OS(R[REG_A0], str, UR[REG_A2]);
            if (alloced_str)
                free(str);
            break;
        }

        /* Unknown system call */
        default:
            fe->run_error("Unknown system call: %d\n", R[REG_SYSCODE]);
            break;
    }

#ifdef _WIN32
    // Reset windows parameter handler
    windowsParameterHandlingControl(1);
#endif

    return true;
}
