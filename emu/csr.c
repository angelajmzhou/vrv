/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "csr.h"
#include "memloader.h"
#include "symbol.h"

/*
 * Exported variables
 */

uregword_t C[CSR_ARRAY_SIZE] = {0};
