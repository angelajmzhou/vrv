/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <ctype.h>
#include <getopt.h>
#include <setjmp.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>

#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#include <termios.h>
#endif

#include "emu.h"
#include "frontend.h"
#include "map.h"
#include "memaccess.h"
#include "param.h"
#include "symbol.h"
#include "tty.h"
#include "utils.h"


/*
 * Structs
 */

typedef struct strlist {
    char *str;
    struct strlist *next;
} strlist_s;

/*
 * CLI arguments
 */

typedef enum climode {
    CLI_RUN = 0,
    CLI_PROMPT,
    CLI_DUMP,
    CLI_FULLDUMP,
} climode_e;

static struct {
    char *prog_name;
    char *system_file;
    int prog_argc;
    char **prog_argv;
    climode_e mode;
    bool assemble_program:1;
} cliargs;


/*
 * CLI commands
 */

typedef enum vrvcmd {
    UNKNOWN_CMD = -1,
    NOP_CMD = 0,
    BREAK_CMD,
    DELETE_CMD,
    HELP_CMD,
    LOAD_CMD,
    PRINT_CMD,
    PRINT_BKPTS_CMD,
    PRINT_REGS_CMD,
    PRINT_SYMBOLS_CMD,
    REDO_CMD,
    QUIT_CMD,
    RUN_CMD,
    SET_CMD,
    STEP_CMD,
    WATCH_CMD,
    WATCH_CLEAR_CMD,
} vrvcmd_e;

static name_val_s cmd_name_map[] = {
#include "cmd_map.h"
};


/*
 * CLI prompt tokens
 */

typedef struct cmdtok {
    enum {
        CMD_NOTOK = -1,
        CMD_NEWLINE,
        CMD_CHAR,
        CMD_INT,
        CMD_PC,
        CMD_REG,
        CMD_CSR,
        CMD_STR,
    } type;

    union {
        int32_t i;
        uint32_t u;
        char *str;
    } val;
} cmdtok_s;

typedef struct cmdtoklist {
    struct cmdtoklist *next;
    cmdtok_s tok;
} cmdtoklist_s;

#define CMD_DELIM(c) \
    (!(c) || isspace(c))


/*
 * Local variables
 */

/* Prompt parsing */
static vrvcmd_e prev_cmd;
#define MAX_LINE_LEN 1024
static char line_buff[MAX_LINE_LEN];
static char *tok_p;     // Points to 1 character after last-parsed token
#define NUM_CMD_ARGS 5
static cmdtok_s cmd_args[NUM_CMD_ARGS];

/* Console state */
jmp_buf vrv_top_lvl_env;
static bool program_loaded;
static bool console_state_saved;
static bool program_running;    /* Used for knowing when to print watch list */
static struct termios saved_console_state;

/* Watch list */
cmdtoklist_s *watch_list;


/*
 * Local functions
 */

/* REPL prompt */
static void top_level(bool run_immediately);

/* Console control transition */
static void console_to_program(void);
static void console_to_vrv(void);

/* CLI arguments */
static int parse_cli_args(int argc, char **argv);

/* CLI frontend IO */
static bool console_input_available(void);
static void control_c_seen(int sig_arg);
static char get_console_char(void);
static void put_console_char(char c);
static bool read_input(char *str, int n);
static void write_output(const char *fmt, ...);
static void write_log(int vlv, const char *fmt, ...);
static void error(const char *fmt, ...);
static void run_error(const char *fmt, ...);
static void fatal_error(const char *fmt, ...);

/* Define CLI frontend struct */
static fe_api_s vrv_fe = {
    .console_input_available = console_input_available,
    .get_console_char = get_console_char,
    .put_console_char = put_console_char,
    .read_input = read_input,
    .write_output = write_output,
    .write_log = write_log,
    .error = error,
    .run_error = run_error,
    .fatal_error = fatal_error,
};

/* Prompt command reading/parsing */
static void flush_to_newline(void);
static bool parse_cmd_token(cmdtok_s *tok);
static vrvcmd_e parse_cmd(void);
static int parse_cmd_args(int num_required, int num_optional, char *expected);
static int boolean_cmd_arg(cmdtok_s *arg);
static bool convert_cmd_arg_symbol(cmdtok_s *arg);
static void inc_cmd_arg(cmdtok_s *arg);
static void exec_cmd(bool force_redo);
static void set_option(cmdtok_s *opt, cmdtok_s *val);

/* Display functions */
static void print_help(void);
static void print_cmd_help(void);
static int print_memory_line(memaddr_t addr);
static void print_register(reg_t reg_no);
static void print_csr(imm_t csr_encoding);
static void print_all_registers(void);
static void print_name_value(char *name, char *value);
static void print_cmdtok(cmdtok_s *tok);
static void print_watch_list(void);
static void format_cmdtok(sstream_s *ss, cmdtok_s *tok);

/* Dump functions */
static int format_memory_line(sstream_s *ss, memaddr_t addr);
static void format_memory_range(sstream_s *ss, memaddr_t lo, memaddr_t hi_ex);
static void format_empty_memory_range(sstream_s *ss, memaddr_t lo, memaddr_t hi_ex);
static void dump_program(void);
static void full_dump_program(void);


/* ========================================================================== *
 * Main Function
 * ========================================================================== */

int main(int argc, char **argv)
{
    // Register frontend IO
    fe = &vrv_fe;

    // Parse arguments
    int prog_i = parse_cli_args(argc, argv);

    // Intialize the emulator
    if (!initialize_world(cliargs.system_file)) {
        // Could not initialize, unrecoverable from CLI
        fe->fatal_error("Aborting...\n");
    }

    // Load any provided programs
    for (; prog_i < argc; ++prog_i) {
        bool file_opened;
        if (!read_program_file(argv[prog_i], &file_opened)) {
            // Could not open or failure while parsing, unrecoverable from CLI
            fe->fatal_error("Aborting...\n");
        }

        // If first program file, load it's name into `prog_argv`
        if (!program_loaded) {
            cliargs.prog_argv[0] = filepath_noext(argv[prog_i]);
            initialize_run_stack(cliargs.prog_argc, cliargs.prog_argv);
            program_loaded = true;
        }
    }

    // Run VRV in the requested mode
    switch (cliargs.mode) {
        case CLI_RUN:
            {
                if (ctl.break_on_exceptions) {
                    // If `break_on_exceptions` and in run mode,
                    //   use prompt but run immediately
                    top_level(true);

                } else {
                    // Run the program with no prompt
                    char *undefs = undefined_symbol_string();
                    if (undefs != NULL) {
                        fe->fatal_error("The following symbols are undefined:\n%s", undefs);
                    }
                    console_to_program();
                    if (!setjmp(vrv_top_lvl_env)) {
                        runres_e res;
                        do {
                            res = exec_program(0);
                            // VRV only needs to care about breakpoint results
                            if (res == RUN_BREAKPOINT) {
                                fe->write_log(1,
                                    "Encountered breakpoint, but ignored in prompt-less run mode, use \"-p\" to enable prompt\n");
                                res = RUN_CONTINUABLE;
                            }
                        } while (res == RUN_CONTINUABLE);
                    }
                    // Ensure we are in correct state after jump
                    ctx.state = EMU_STATE;
                }
                break;
            }
        case CLI_PROMPT:
            {
                top_level(false);
                break;
            }
        case CLI_DUMP:
            {
                dump_program();
                break;
            }
        case CLI_FULLDUMP:
            {
                full_dump_program();
                break;
            }
    }

    console_to_vrv();
    return ctx.vrv_exit_code;
}


/* ========================================================================== *
 * REPL Prompt
 * ========================================================================== */

static void top_level(bool run_immediately)
{
    // Sort command table
    qsort(cmd_name_map, ARRAY_SIZE(cmd_name_map), sizeof(name_val_s), (comp_f)__compare_name);

    // Set breaking signal for ctrl+c
    (void)signal(SIGINT, control_c_seen);

    // If should `run_mmediately`, just once, prep a "redo" run command
    if (run_immediately) {
        prev_cmd = RUN_CMD;
        if (!setjmp(vrv_top_lvl_env))
            exec_cmd(true);
        ctx.state = EMU_STATE;  // Ensure we are in correct state after jump
    }

    // repl
    prev_cmd = NOP_CMD;
    while (1) {
        console_to_vrv();
        fflush(stdout);
        fflush(stderr);
        fe->error("(vrv) ");
        if (!setjmp(vrv_top_lvl_env))
            exec_cmd(false);
        ctx.state = EMU_STATE;  // Ensure we are in correct state after jump
        if (program_running) {
            program_running = false;
            print_watch_list();
        }
    }
}


/* ========================================================================== *
 * Console Control Transfer
 * ========================================================================== */

/* Give the console to the program for IO. */
static void console_to_program(void)
{
    if (conf.mapped_tty && !console_state_saved) {
        struct termios params;

        tcgetattr(STDIN_FILENO, &saved_console_state);
        params = saved_console_state;
        params.c_iflag &=
            ~(ISTRIP | INLCR | ICRNL | IGNCR | IXON | IXOFF | INPCK | BRKINT
            | PARMRK);

        // Translate CR -> NL to canonicalize input
        params.c_iflag |= IGNBRK | IGNPAR | ICRNL;
        params.c_oflag = OPOST | ONLCR;
        params.c_cflag &= ~PARENB;
        params.c_cflag |= CREAD | CS8;
        params.c_lflag = 0;
        params.c_cc[VMIN] = 1;
        params.c_cc[VTIME] = 1;

        tcsetattr(STDIN_FILENO, TCSANOW, &params);
        console_state_saved = true;
    }
}

static void console_to_vrv(void)
{
    if (conf.mapped_tty && console_state_saved)
        tcsetattr(STDIN_FILENO, TCSANOW, &saved_console_state);
    console_state_saved = false;
}


/* ========================================================================== *
 * CLI Frontend IO
 * ========================================================================== */

/* Returns true if input is available for MMIO.
 * Used to check when to signal IO interupts. */
static bool console_input_available(void)
{
    fd_set fdset;
    struct timeval timeout;

    if (conf.mapped_tty) {
        timeout.tv_sec = 0;
        timeout.tv_usec = 0;
        FD_ZERO(&fdset);
        FD_SET(STDIN_FILENO, &fdset);
        return select(sizeof(fdset) * 8, &fdset, NULL, NULL, &timeout);
    } else {
        return 0;
    }
}

static void control_c_seen(int sig_arg)
{
    (void)sig_arg;
    console_to_vrv();
    if (ctx.state == EXEC_STATE) {
        fe->error("\nExecution interrupted\n");
        longjmp(vrv_top_lvl_env, 1);
    } else {
        fe->error("\nExiting...\n");
        exit(0);
    }
}

static char get_console_char(void)
{
    char buf;

    (void)read(STDIN_FILENO, &buf, 1);

    if (buf == 3)
        control_c_seen(0);
    return buf;
}

static void put_console_char(char c)
{
    putc(c, stdout);
    fflush(stdout);
}

static bool read_input(char *str, int n)
{
    char *ptr;
    int restore_console_to_program = 0;

    if (console_state_saved) {
        restore_console_to_program = 1;
        console_to_vrv();
    }

    ptr = str;

    while (1 < n) {      // Reserve space for null
        char buf[1];
        if (read((int)STDIN_FILENO, buf, 1) <= 0) // Not in raw mode!
            break;

        *ptr++ = buf[0];
        n -= 1;

        if (buf[0] == '\n')
            break;
    }

    if (0 < n)
        *ptr = '\0';                /* Null terminate input */

    if (restore_console_to_program)
        console_to_program();

    return true; // Blocking, so the input will always be read fully
}

static void write_output(const char *fmt, ...)
{
    va_list args;
    bool restore_console_to_program = false;

    va_start(args, fmt);

    if (console_state_saved) {
        restore_console_to_program = true;
        console_to_vrv();
    }

    vfprintf(stdout, fmt, args);
    fflush(stdout);

    va_end(args);

    if (restore_console_to_program)
        console_to_program();
}

static void write_log(int vlv, const char *fmt, ...)
{
    if (vlv > ctl.verbose_lvl)
        return;

    va_list args;
    bool restore_console_to_program = false;

    va_start(args, fmt);

    if (console_state_saved) {
        restore_console_to_program = true;
        console_to_vrv();
    }

    vfprintf(stderr, fmt, args);
    fflush(stdout);

    va_end(args);

    if (restore_console_to_program)
        console_to_program();
}

static void error(const char *fmt, ...)
{
    va_list args;
    bool restore_console_to_program = false;

    va_start(args, fmt);

    if (console_state_saved) {
        restore_console_to_program = true;
        console_to_vrv();
    }

    vfprintf(stderr, fmt, args);
    fflush(stderr);

    va_end(args);

    if (restore_console_to_program)
        console_to_program();
}

static void run_error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    console_to_vrv();

    vfprintf(stderr, fmt, args);
    fflush(stderr);

    va_end(args);

    longjmp(vrv_top_lvl_env, 1);
}

static void fatal_error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    console_to_vrv();

    vfprintf(stderr, fmt, args);
    fflush(stderr);

    va_end(args);

    exit(-1);
}


/* ========================================================================== *
 * CLI Arguments
 * ========================================================================== */

static struct option long_options[] = {
    { "arg",        required_argument,    NULL,   'a' },
    { "break",      no_argument,          NULL,   'b' },
    { "dump",       no_argument,          NULL,   'd' },
    { "fulldump",   no_argument,          NULL,   'D' },
    { "help",       no_argument,          NULL,   'h' },
    { "misalign",   optional_argument,    NULL,   'm' },
    { "prompt",     no_argument,          NULL,   'p' },
    { "quiet",      no_argument,          NULL,   'q' },
    { "system",     required_argument,    NULL,   's' },
    { "tty_mapped", no_argument,          NULL,   't' },
    { "verbose",    no_argument,          NULL,   'v' },
    { 0, 0, 0, 0 }
};

static int parse_cli_args(int argc, char **argv)
{
    strlist_s *prog_args_head = NULL;
    strlist_s *sl;
    int option_index;

    cliargs.prog_name = argv[0];

    opterr = 0; // Manage option errors ourselves

    for(;;) {
        int c = getopt_long(argc, argv, ":a:bdDhm:ps:qvt",
                            long_options, &option_index);

        if (c == -1)
            // No more options
            break;

        switch(c) {
            /* --arg <argument> */
            case 'a':
                sl = (strlist_s *)xmallocz(sizeof(strlist_s));
                sl->str = optarg;
                sl->next = prog_args_head;
                prog_args_head = sl;
                ++cliargs.prog_argc;
                break;

            /* --break */
            case 'b':
                ctl.break_on_exceptions = true;
                break;

            /* --dump */
            case 'd':
                cliargs.mode = CLI_DUMP;
                break;

            /* --fulldump */
            case 'D':
                cliargs.mode = CLI_FULLDUMP;
                break;

            /* --help */
            case 'h':
                print_help();
                exit(0);

            /* --misalign load|store> */
            case 'm':
                if (!strcmp_ci(optarg, "load"))
                    ctl.misaligned_exceptions &= ~MEMOP_LOAD;
                else if(!strcmp_ci(optarg, "store"))
                    ctl.misaligned_exceptions &= ~MEMOP_STORE;
                else if(!strcmp_ci(optarg, "all"))
                    ctl.misaligned_exceptions &= ~(MEMOP_LOAD | MEMOP_STORE);
                else
                    fe->fatal_error("Invalid argument for '-m' option: %s\n", optarg);
                break;

            /* --prompt  */
            case 'p':
                cliargs.mode = CLI_PROMPT;
                break;

            /* --quiet */
            case 'q':
                ctl.verbose_lvl = 0;
                break;

            /* --system <file> */
            case 's':
                cliargs.system_file = optarg;
                break;

            /* --verbose */
            case 'v':
                ctl.verbose_lvl = 2;
                break;

            /* --tty_mapped */
            case 't':
                conf.mapped_tty = true;
                break;

            /* Missing option argument */
            case ':':
                fe->fatal_error("Missing argument for option '-%c'\n", optopt);
                break;

            /* Invalid option */
            case '?':
            default:
                if (optopt == '\0')
                    fe->error("Invalid option \"%s\"\n", argv[optind - 1]);
                else
                    fe->error("Invalid option '-%c'\n", optopt);
                print_help();
                exit(-1);
                break;
        }
    }

    // Determine if the prompt should be implicitly used
    bool any_prog_files = optind < argc;
    if (cliargs.mode == CLI_RUN && !any_prog_files && !cliargs.system_file)
        cliargs.mode = CLI_PROMPT;

    // Ensure there is an exception file by using default
    if (!cliargs.system_file)
        cliargs.system_file = DEFAULT_SYSTEM_FILE;

    // Emit error if attempting to dump (not fulldump) without program files
    if (!any_prog_files && cliargs.mode == CLI_DUMP)
        fe->fatal_error("Cannot dump user segments, no program files provided\n");

    // Construct `prog_argv` from strlist (`prog_args` is in reversed order)
    /* @HELP: Not sure how we should determine program name for argv
     *        now that we allow multiple program files... */
    ++cliargs.prog_argc;    // Leave room for program name
    cliargs.prog_argv = (char **)xmalloc(cliargs.prog_argc * sizeof(char *));
    cliargs.prog_argv[0] = "";
    for (int i = cliargs.prog_argc - 1; i > 0; --i) {
        cliargs.prog_argv[i] = prog_args_head->str;

        strlist_s *to_free = prog_args_head;
        prog_args_head = prog_args_head->next;
        free(to_free);
    }

    // Return index of first non-option argument
    return optind;
}


/* ========================================================================== *
 * Prompt Command Reading/Parsing
 * ========================================================================== */

/* Flush the rest of the input line up to and including the next newline */
static void flush_to_newline()
{
    cmdtok_s tok;
    do {
        parse_cmd_token(&tok);
    } while (tok.type != CMD_NEWLINE);
}

/* Parse token from the prompt input */
static bool parse_cmd_token(cmdtok_s *tok)
{
    do {
        if (!tok_p) {
            // Ready to read new line
            tok_p = line_buff;
            if (!fgets(line_buff, MAX_LINE_LEN, stdin)) {
                // EOF has been reached
                console_to_vrv();
                exit(0);
            }
        }

        // Skip leading whitespace (do not skip newlines)
        while (*tok_p && *tok_p != '\n' && isspace(*tok_p))
            ++tok_p;

    // Repeat "read line" then "skip whitespace" until valid character seen
    } while (!(*tok_p));

    // Parse next token
    char *tok_start = tok_p, *tok_end = NULL;
    char c1 = tolower(*tok_start), c2 = tolower(*(tok_start + 1));

    // Perform different action based on first two characters
    // If the parse succeeded then it will return out of the conditional,
    //   so it can be assumed that if we leave the conditional, then that type
    //   of parse failed.
    // Must always update `tok_p` before exiting function
    if (c1 == '"') {
        fe->error("VRV prompt does not support double-quoted strings\n");
        tok_p = tok_start + 1;  // Ensure we increment to prevent inf loop
        return false;
    }

    if (c1 == '\n') {
        // - Newline
        tok->type = CMD_NEWLINE;
        tok_p = NULL;   // This signals to read a new line
        return true;
    }

    if (c1 == '?' || c1 == ':') {
        // - Special command character
        tok->type = CMD_CHAR;
        tok->val.i = c1;
        tok_p = tok_start + 1;
        return true;
    }

    if (isdigit(c1) || (c1 == '-' && isdigit(c2))) {
        // - Integer
        tok->type = CMD_INT;
        // Use radix=0 to parse both dec and hex (when prefixed by "0x")
        tok->val.u = strtol(tok_start, &tok_end, 0);
        // Ensure `tok_end` points to delim before succeeding
        if (CMD_DELIM(*tok_end)) {
            tok_p = tok_end;
            return true;
        }
    }

    // Check for addressing from register values
    bool reg_addr = c1 == '@' && c2 != '\0'; // Ensure c2+1 will be valid
    if (reg_addr) {
        ++tok_start;
        c1 = c2;
        c2 = tolower(*(tok_start + 1));
    }

    if ((c1 == 'x' || c1 == 'f') && isdigit(c2)) {
        // - Integer/Float register
        long val = strtol(tok_start + (reg_addr ? 2 : 1), &tok_end, 10);
        // Ensure reg in range and `tok_end` points to delim before succeeding
        if (val >= 0 && val < R_LENGTH && CMD_DELIM(*tok_end)
                // Also reject non-canonical forms such as `x02`
                && (val == 0 || c2 > '0')) {
            if (reg_addr) {
                if (c1 == 'f') {
                    fe->error("Cannot use FP register as address\n");
                    return false;
                }
                tok->type = CMD_INT;
                tok->val.i = R[val];
            } else {
                tok->type = CMD_REG;
                tok->val.i = val | (c1 == 'f' ? FREG_BIT : 0);
            }
            tok_p = tok_end;
            return true;
        }
    }

    // For all upcoming tests, we must grab string first
    tok_end = tok_start + 1;
    while (!CMD_DELIM(*tok_end))
        ++tok_end;
    char c_end = *tok_end;
    *tok_end = '\0';
    char *tok_str = xstrdup(tok_start);
    *tok_end = c_end;
    tok_p = tok_end;  // Will always succeed from this point, so set tok_p here

    /* Search by ABI name for GPR registers */
    name_val_s *reg_entry = SEARCH_GPR_NAME(tok_str);
    if (reg_entry) {
        // ABI register name
        if (reg_addr) {
            tok->type = CMD_INT;
            tok->val.i = R[reg_entry->val];
        } else {
            tok->type = CMD_REG;
            tok->val.i = reg_entry->val;
        }
        free(tok_str);
        return true;
    }

    /* Search by ABI name for FPR registers */
    reg_entry = SEARCH_FPR_NAME(tok_str);
    if (reg_entry) {
        // ABI register name
        if (reg_addr) {
            fe->error("Cannot use FP register as address\n");
            free(tok_str);
            return false;
        } else {
            tok->type = CMD_REG;
            tok->val.i = reg_entry->val;
            free(tok_str);
            return true;
        }
    }

    csr_name_s *csr_name = SEARCH_CSR_NAME(tok_str);
    if (csr_name) {
        // - CSR name
        if (reg_addr) {
            csr_encoding_s *csr_enc = SEARCH_CSR_ENCODING(csr_name->encoding);
            tok->type = CMD_INT;
            tok->val.i = get_csr_field(csr_enc->index, csr_enc->shift, csr_enc->mask);
;
        } else {
            tok->type = CMD_CSR;
            // Use CSR encoding so that the entry can be looked up
            tok->val.i = csr_name->encoding;
        }
        free(tok_str);
        return true;
    }

    // Special string cases
    if (!strcmp_ci(tok_str, "pc")) {
        // - PC, use its value as an address, but remember the token
        tok->type = CMD_PC;
        tok->val.u = PC;
        free(tok_str);
        return true;
    }

    // Otherwise, it's a string
    tok->type = CMD_STR;
    tok->val.str = tok_str;
    return true;
}

/* Read the first token in the line and determine which command was requested */
static vrvcmd_e parse_cmd(void)
{
    cmdtok_s tok;
    if (!parse_cmd_token(&tok))
        return UNKNOWN_CMD;

    // Check special cases
    if (tok.type == CMD_NEWLINE)
        return REDO_CMD;
    if (tok.type == CMD_CHAR) {
        switch (tok.val.i) {
            case '?':
                return HELP_CMD;
            case ':':
                return SET_CMD;
            default:
                return UNKNOWN_CMD;
        }
    }

    // Search in command map
    if (tok.type != CMD_STR)
        return UNKNOWN_CMD;
    name_val_s key = { tok.val.str, 0 };
    name_val_s *entry = bsearch(&key, cmd_name_map, ARRAY_SIZE(cmd_name_map),
        sizeof(name_val_s), (comp_f)__compare_name);

    free(tok.val.str);
    return entry ? entry->val : UNKNOWN_CMD;
}

#define CMD_ERROR(fmt, ...)      \
do {                             \
    fe->error(fmt, __VA_ARGS__); \
    prev_cmd = NOP_CMD;          \
} while (0)

/* Fill `cmd_args` array using the given number of required and optional arguments.
 * Returns -1 on error (missing or excess arguments).
 * Otherwise, returns the number arguments parsed
 *   (useful when `num_optional` is non-zero).
 * Always ensures the line is fully parsed after the call. */
static int parse_cmd_args(int num_required, int num_optional, char *expected)
{
    // Free any old cmd_args strings
    for (int a = 0; a < NUM_CMD_ARGS; ++a) {
        if (cmd_args[a].type == CMD_STR) {
            free(cmd_args[a].val.str);
        }
        cmd_args[a].val.str = NULL;
        cmd_args[a].type = -1;
    }

    int i = 0;
    // Parse one more than required in order to catch newline (or excess)
    for (; i <= num_required + num_optional; ++i) {
        if (!parse_cmd_token(&cmd_args[i])) {
            flush_to_newline();
            return -1;
        }

        if (cmd_args[i].type == CMD_NEWLINE) {
            if (i < num_required) {
                CMD_ERROR("Missing arguments, expected: %s\n", expected);
                return -1;
            }
            return i;
        }
    }

    CMD_ERROR("Excess arguments, expected: %s\n", expected);
    flush_to_newline();
    return -1;
}

/* Interpret a boolean command in either numerical form or string form */
static int boolean_cmd_arg(cmdtok_s *arg)
{
    switch (arg->type) {
        case CMD_INT:
            if (arg->val.i)
                return 1;
            return 0;
        case CMD_STR:
            if (   str_is_ciprefix(arg->val.str, "true", 1)
                || str_is_ciprefix(arg->val.str, "yes", 1))
                return 1;
            if (   str_is_ciprefix(arg->val.str, "false", 1)
                || str_is_ciprefix(arg->val.str, "no", 1))
                return 0;
            return -1;
        default:
            return -1;
    }
}

/* Shortcut for finding and converting string args to address args
 * Returns false only if symbol is undefined */
static bool convert_cmd_arg_symbol(cmdtok_s *arg)
{
    if (arg->type != CMD_STR)
        return true;

    label_s *label_list = lookup_all_matching_defined_labels(arg->val.str);
    if (!label_list)
        return false;

    /* Multiple labels, report the unused addresses and pick the first one */
    /* @TODO: Find more elegant way to handle multiple labels*/
    memaddr_t addr = 0;
    if (label_list->next) {
        label_s *label = label_list;
        fe->write_log(1, "  Found multiple symbols with the same name:\n");
        fe->write_log(1, "    %s:  0x%08x  <-- using this address\n",
            label->name, label->addr);
        addr = label->addr;

        label = label->next;
        while (label) {
            fe->write_log(1, "    %s:  0x%08x\n", label->name, label->addr);
            label = label->next;
        }

    } else {
        addr = label_list->addr;
    }
    destroy_lookup_list(label_list);

    free(arg->val.str);
    arg->type = CMD_INT;
    arg->val.u = addr;
    return true;
}

/* Increment the value of the given argument for `redo` progression */
static void inc_cmd_arg(cmdtok_s *arg)
{
    switch (arg->type) {
        case CMD_INT:
            if (is_text_addr(arg->val.u))
                arg->val.u += BP_WORD;
            else
                arg->val.u += BP_QWORD;
            break;
        case CMD_PC:
            arg->val.u += BP_WORD;
            break;
        case CMD_REG:
            if (arg->val.u & FREG_BIT)
                arg->val.u = ((arg->val.u + 1) % FR_LENGTH) | FREG_BIT;
            else
                arg->val.u = (arg->val.u + 1) % R_LENGTH;
            break;
        default: {}
    }
}

/* Read, parse, and execute prompt command and arguments */
static void exec_cmd(bool force_redo)
{
    vrvcmd_e cmd;
    bool redo;

    // Either force a redo or read the command
    if (force_redo) {
        redo = true;
    } else {
        cmd = parse_cmd();
        // Determine if redo here
        redo = cmd == REDO_CMD;
    }

    if (redo)
        cmd = prev_cmd;
    else
        prev_cmd = cmd;

    switch (cmd) {
        case NOP_CMD:
            return;

        case BREAK_CMD:
            {
                char *expected = "breakpoint <ADDR|PC|SYMBOL>";
                if (!redo && parse_cmd_args(1, 0, expected) < 0)
                    return;

                // Convert symbol address to CMD_INT type token
                if (!convert_cmd_arg_symbol(&cmd_args[0])) {
                    CMD_ERROR("Symbol is undefined: %s\n", cmd_args[0].val.str);
                    /* Leftover arg strings freed during parsing */
                    return;
                }

                // Validate token type
                if (cmd_args[0].type != CMD_INT && cmd_args[0].type != CMD_PC) {
                    CMD_ERROR("Invalid argument, expected: %s\n", expected);
                    return;
                }

                // Add breakpoint
                memaddr_t bkpt_addr = cmd_args[0].val.u;
                int bkpt_id = add_breakpoint(bkpt_addr);
                if (bkpt_id >= 0)
                    fe->error("Breakpoint added with ID=(%d) at [0x%08x]\n",
                        bkpt_id, bkpt_addr);
                cmd_args[0].val.u += BP_WORD;   // Increment for redo command
                break;
            }

        case DELETE_CMD:
            {
                char *expected = "delete [ADDR|PC|SYMBOL]";
                int num_args = -1;
                if (!redo && (num_args = parse_cmd_args(0, 1, expected)) < 0)
                    return;

                // Parse first token to find symbol or address
                if (num_args == 0) {
                    // No argument given, delete all breakpoints
                    int num_deleted = delete_all_breakpoints();
                    fe->error("Deleted all %d breakpoints\n", num_deleted);

                } else if (cmd_args[0].type == CMD_INT && cmd_args[0].val.u < SEG_GUARD) {
                    // Argument is breakpoint ID, handle differently
                    memaddr_t deleted_addr = delete_breakpoint_id(cmd_args[0].val.u);
                    if (deleted_addr)
                        fe->error("Breakpoint deleted with ID=(%d) at [0x%08x]\n",
                            cmd_args[0].val.u, deleted_addr);
                    cmd_args[0].val.u += 1;     // Increment for redo command

                } else {
                    // Convert symbol address to CMD_INT type token
                    if (!convert_cmd_arg_symbol(&cmd_args[0])) {
                        CMD_ERROR("Symbol is undefined: %s\n", cmd_args[0].val.str);
                        /* Leftover arg strings freed during parsing */
                        return;
                    }

                    // Validate token type
                    if (cmd_args[0].type != CMD_INT && cmd_args[0].type != CMD_PC) {
                        CMD_ERROR("Invalid argument, expected: %s\n", expected);
                        return;
                    }

                    // Delete breakpoint
                    memaddr_t bkpt_addr = cmd_args[0].val.u;
                    int deleted_id = delete_breakpoint(bkpt_addr);
                    if (deleted_id >= 0)
                        fe->error("Breakpoint deleted with ID=(%d) at [0x%08x]\n",
                            deleted_id, bkpt_addr);
                    cmd_args[0].val.u += BP_WORD;   // Increment for redo command
                }

                break;
            }

        case HELP_CMD:
            {
                if (!redo && parse_cmd_args(0, 0, "help") < 0)
                    return;

                print_cmd_help();
                break;
            }

        case LOAD_CMD:
            {
                char *expected = "load <FILENAME>";
                if (!redo && parse_cmd_args(1, 0, expected) < 0)
                    return;

                if (cmd_args[0].type != CMD_STR) {
                    CMD_ERROR("Invalid argument, expected: %s\n", expected);
                    return;
                }

                bool file_opened;
                if (!read_program_file(cmd_args[0].val.str, &file_opened)
                        && file_opened) { // <-- Only abort if the file opened
                    // Failure while parsing, unrecoverable from CLI
                    fe->fatal_error("Aborting...\n");
                }
                // If first program file, load it's name into `prog_argv`
                if (!program_loaded) {
                    cliargs.prog_argv[0] = filepath_noext(cmd_args[0].val.str);
                    initialize_run_stack(cliargs.prog_argc, cliargs.prog_argv);
                    program_loaded = true;
                }
                break;
            }

        case PRINT_CMD:
            {
                char *expected = "print <REG|FREG|CSR|ADDR|PC|SYMBOL>";
                if (!redo && parse_cmd_args(1, 0, expected) < 0)
                    return;

                // Convert symbol address to CMD_INT type token
                if (!convert_cmd_arg_symbol(&cmd_args[0])) {
                    CMD_ERROR("Symbol is undefined: %s\n", cmd_args[0].val.str);
                    /* Leftover arg strings freed during parsing */
                    return;
                }

                // Validate token type
                switch (cmd_args[0].type) {
                    case CMD_INT: case CMD_PC: case CMD_REG: case CMD_CSR:
                        break;
                    default:
                        CMD_ERROR("Invalid argument, expected: %s\n", expected);
                        return;
                }

                // Print argument
                print_cmdtok(&cmd_args[0]);

                // Incrememnt value for redo
                inc_cmd_arg(&cmd_args[0]);

                break;
            }

        case PRINT_BKPTS_CMD:
            {
                if (!redo && parse_cmd_args(0, 0, "printregisters") < 0)
                    return;

                print_breakpoints();
                break;
            }

        case PRINT_REGS_CMD:
            {
                if (!redo && parse_cmd_args(0, 0, "printregisters") < 0)
                    return;

                print_all_registers();
                break;
            }

        case PRINT_SYMBOLS_CMD:
            {
                char *expected = "printsymbols [PRINT_RETIRED?]";
                int num_args = -1;
                if (!redo && (num_args = parse_cmd_args(0, 1, expected)) < 0)
                    return;

                int print_retired = 0;
                if (num_args > 0 && (print_retired = boolean_cmd_arg(&cmd_args[0])) < 0) {
                    CMD_ERROR("Invalid argument, expected: %s\n", expected);
                    return;
                }

                print_symbols(print_retired);
                break;
            }

        case QUIT_CMD:
            console_to_vrv();
            exit(0);

        case RUN_CMD:
            {
                if (!redo && parse_cmd_args(0, 0, "run") < 0)
                    return;

                char *undefs = undefined_symbol_string();
                if (undefs != NULL) {
                    fe->error("The following symbols are undefined:\n%s", undefs);
                    free(undefs);
                    return;
                }
                console_to_program();
                program_running = true;
                runres_e res = exec_program(0);
                // VRV only needs to care about breakpoint results
                if (res == RUN_BREAKPOINT)
                    fe->error("Breakpoint encountered at 0x%08x\n", PC);
                console_to_vrv();
                break;
            }

        case SET_CMD:
            {
                if (!redo && parse_cmd_args(2, 0, "set <OPTION> <VALUE>") < 0)
                    return;

                set_option(&cmd_args[0], &cmd_args[1]);
                break;
            }

        case STEP_CMD:
            {
                char *expected = "step [N]";
                int num_args = -1;
                if (!redo && (num_args = parse_cmd_args(0, 1, expected)) < 0)
                    return;

                int num_steps = 1;
                if (num_args > 0) {
                    if (cmd_args[0].type != CMD_INT) {
                        CMD_ERROR("Invalid argument, expected: %s\n", expected);
                        return;
                    }
                    num_steps = cmd_args[0].val.i;
                    if (num_steps <= 0) {
                        CMD_ERROR("Invalid number of steps, `%d` < 1\n", num_steps);
                        return;
                    }
                }

                char *undefs = undefined_symbol_string();
                if (undefs != NULL) {
                    fe->error("The following symbols are undefined:\n%s", undefs);
                    free(undefs);
                }
                console_to_program();
                program_running = true;
                runres_e res = exec_program(num_steps);
                // VRV only needs to care about breakpoint results
                if (res == RUN_BREAKPOINT)
                    fe->error("Breakpoint encountered at 0x%08x\n", PC);
                console_to_vrv();
                break;
            }

        case WATCH_CMD:
            {
                char *expected = "watch <REG|FREG|CSR|ADDR|PC|SYMBOL>";
                if (!redo && (parse_cmd_args(1, 0, expected)) < 0)
                    return;

                /* Align non-text addresses */
                if (cmd_args[0].type == CMD_INT) {
                    if (is_text_addr(cmd_args[0].val.u))
                        cmd_args[0].val.u &= ~(BP_WORD - 1);
                    else
                        cmd_args[0].val.u &= ~(BP_QWORD - 1);
                }

                /* Convert symbol address to CMD_INT type token */
                if (!convert_cmd_arg_symbol(&cmd_args[0])) {
                    CMD_ERROR("Symbol is undefined: %s\n", cmd_args[0].val.str);
                    return;
                }

                /* Validate token types */
                switch (cmd_args[0].type) {
                    case CMD_INT: case CMD_PC: case CMD_REG: case CMD_CSR:
                        break;
                    default:
                        CMD_ERROR("Invalid argument, expected: %s\n", expected);
                        return;
                }

                /* Look for match while traverseing to the end */
                bool found = false;
                cmdtoklist_s *prev = NULL, *iter = watch_list;
                while (!found && iter) {
                    if (cmd_args[0].type == iter->tok.type) {
                        /* Watch considers PC a unique token */
                        found |= cmd_args[0].type == CMD_PC;
                        /* Otherwise, all valid tokens have integer value */
                        found |= cmd_args[0].val.u == iter->tok.val.u;
                    }
                    if (!found) {
                        prev = iter;
                        iter = iter->next;
                    }
                }

                sstream_s ss;
                ss_init(&ss);
                if (found) {
                    /* Found a match, remove it from the watch list */
                    ss_printf(&ss, "Stopped watching: ");
                    format_cmdtok(&ss, &(iter->tok));

                    if (prev)
                        prev->next = iter->next;
                    else
                        watch_list = iter->next;
                    free(iter);

                } else {
                    /* Iterated to the end, append to list */
                    cmdtoklist_s *to_append = xmallocz(sizeof(cmdtoklist_s));
                    to_append->tok = cmd_args[0];   /* Copy token into list */
                    if (prev)
                        prev->next = to_append;
                    else
                        watch_list = to_append;

                    ss_printf(&ss, "Started watching: ");
                    format_cmdtok(&ss, &(to_append->tok));
                }

                fe->write_log(0, "%s\n", ss_to_string(&ss));
                ss_destroy(&ss);

                /* Increment arg value */
                inc_cmd_arg(&cmd_args[0]);

                break;
            }

        case WATCH_CLEAR_CMD:
            {
                char *expected = "watchclear";
                if (!redo && (parse_cmd_args(0, 0, expected)) < 0)
                    return;

                int count = 0;
                while (watch_list) {
                    cmdtoklist_s *to_free = watch_list;
                    watch_list = watch_list->next;
                    free(to_free);
                    ++count;
                }
                fe->write_log(0, "Cleared %d item(s) from watch list\n", count);
                break;
            }

        default:
            {
                CMD_ERROR("Unknown command\n", 0);
                flush_to_newline();
                return;
            }
    }
}

static void set_option(cmdtok_s *opt, cmdtok_s *val)
{
    if (opt->type != CMD_STR) {
        fe->error("Invalid option name\n");
        return;
    }

    char *opt_str = opt->val.str;
    if (str_is_ciprefix(opt_str, "abi_register_names", 1)) {
        char *expected = "abi_register_names (0|1)";

        int val_bool = boolean_cmd_arg(val);
        if (val_bool < 0) {
            fe->error("Invalid boolean value, expected: %s\n", expected);
            return;
        }
        ctl.abi_register_names = val_bool;

    } else if (str_is_ciprefix(opt_str, "break_on_exceptions", 1)) {
        char *expected = "break_on_exceptions (0|1)";

        int val_bool = boolean_cmd_arg(val);
        if (val_bool < 0) {
            fe->error("Invalid boolean value, expected: %s\n", expected);
            return;
        }
        ctl.break_on_exceptions = val_bool;

    } else if (str_is_ciprefix(opt_str, "misaligned_exceptions", 1)) {
        char *expected = "misaligned_exceptions (load|store|all|none)";

        if (val->type != CMD_STR) {
            fe->error("Invalid value, expected: %s\n", expected);
            return;
        }
        char *val_str = val->val.str;
        if (     str_is_ciprefix(val_str, "load", 1))
            ctl.misaligned_exceptions = MEMOP_LOAD;
        else if (str_is_ciprefix(val_str, "store", 1))
            ctl.misaligned_exceptions = MEMOP_STORE;
        else if (str_is_ciprefix(val_str, "all", 1))
            ctl.misaligned_exceptions = MEMOP_LOAD | MEMOP_STORE;
        else if (str_is_ciprefix(val_str, "none", 1))
            ctl.misaligned_exceptions = 0;
        else {
            fe->error("Unknown value, expected: %s\n", expected);
            return;
        }

    } else if (str_is_ciprefix(opt_str, "verbose_level", 1)) {
        char *expected = "verbose_level (0-2)";

        if (val->type != CMD_INT || val->val.i < 0 || val->val.i > 2) {
            fe->error("Invalid value, expected: %s\n", expected);
            return;
        }
        ctl.verbose_lvl = val->val.i;

    } else {
        fe->error("Unknown option name\n");
        return;
    }
}


/* ========================================================================== *
 * Display Functions
 * ========================================================================== */

static void print_help(void)
{
    fe->error("Usage: %s [options] [program_file...]\n\n\
-a, --arg <ARGUMENT>             Add a program argument\n\
-b, --break                      Break to the prompt on exceptions\n\
-d, --dump                       Dump user text+data segments, instead of running\n\
-D, --fulldump                   Dump user+kernel text+seg segments, instead of running\n\
-h, --help                       Print this message\n\
-m, --misalign (load|store|all)  Do NOT raise exception on misaligned memory accesses\n\
-p, --prompt                     Enter the interatctive prompt, instead of running\n\
-q, --quiet                      Silence all log messages (still prints errors/exceptions)\n\
-s, --system <FILE>              Load <file> as the system code instead of the builtin\n\
-t, --tty_mapped                 Use memory-mapped TTY IO device\n\
-v, --verbose                    Print extra logging messages (instructions as they execute)\n"
"\nCopyright (c) 2023 LupLab\nGNU Affero General Public License version 3\n",
        cliargs.prog_name);
}

static void print_cmd_help(void)
{
    fe->error("\
  b, breakpoint <ADDR|PC|SYMBOL>\n\
      Add a breakpoint at the given address\n\
  d, delete [ADDR|PC|SYMBOL|BRKPT_ID]\n\
      Delete the breakpoint at the given address or with the given ID\n\
      No argument causes all breakpoints to be deleted\n\
  h, ?, help\n\
      Print this help message\n\
  l, load <FILENAME>\n\
      Load the given program file\n\
  p, print <REG|FREG|CSR|ADDR|PC|SYMBOL>\n\
      Print information about the given register or memory location\n\
      Prefix a register with '@' to use memory location contained in the reigster\n\
  pb, printbreakpoints\n\
      Print information about all breakpoints created from the prompt\n\
  pr, printregisters\n\
      Print value of all memory registers and CSRs\n\
  ps, printsymbols [PRINT_RETIRED?]\n\
      Print all global (and retired if flag set) symbols and their addresses\n\
  q, quit\n\
      Quit VRV\n\
  r, run\n\
      Run the program for either the current PC or from \"%s\" if its the first run\n\
  s, step [N]\n\
      Step through the program for [n] instructions\n\
  w, watch <REG|FREG|CSR|ADDR|PC|SYMBOL>\n\
      Call `print` with the given argument after each `run` or `step` command\n\
      Stops printing if already watching (toggles)\n\
  wc, watchclear\n\
      Clear watch list\n\
  <Enter>\n\
      Redo last command. Certain commands may increment their arguments.\n\
  :, set <OPTION> <VALUE>\n\
      Set <OPTION> to <VALUE>, can use unique prefix of any option name.\n\
      The following options are available:\n\
          abi_register_names (0|1)\n\
              Use ABI register names when printing\n\
          break_on_exceptions (0|1)\n\
              Break to prompt when excepitons occur\n\
          misaligned_exceptions (load|store|all|none)\n\
              Raise exceptions on misaligned memory accesses\n\
          verbose_level (0-2)\n\
              Verbosity of reported logs (see -q and -v)\n");
}

static int print_memory_line(memaddr_t addr)
{
    sstream_s ss;
    ss_init(&ss);
    int bytes_printed = format_memory_line(&ss, addr);
    if (bytes_printed > 0)
        fe->write_log(0, "%s", ss_to_string(&ss));
    ss_destroy(&ss);
    return bytes_printed;
}

static void print_register(reg_t reg_no)
{
    sstream_s ss_name;
    sstream_s ss_value;
    ss_init(&ss_name);
    ss_init(&ss_value);

    // Print either floating-point or integer register value
    if (reg_no & FREG_BIT) {
        reg_no -= FREG_BIT;
        ss_printf(&ss_name, fe_fpr_names[reg_no]);
        floatbits_u fb;
        fb.f = FR[reg_no];
        ss_printf(&ss_value, "0x%08x  (%f)", fb.b, fb.f);
    } else {
        ss_printf(&ss_name, fe_gpr_names[reg_no]);
        ss_printf(&ss_value, "0x%08x  (%d)", R[reg_no], R[reg_no]);
    }

    print_name_value(ss_to_string(&ss_name), ss_to_string(&ss_value));
    ss_destroy(&ss_name);
    ss_destroy(&ss_value);
}

static void print_csr(imm_t csr_encoding)
{
    csr_encoding_s *entry = SEARCH_CSR_ENCODING(csr_encoding);
    if (!entry) {
        fe->write_log(0, "Unrecognized CSR\n");
        return;
    }

    sstream_s ss;
    ss_init(&ss);
    ss_printf(&ss, "0x%08x", get_csr_field(entry->index, entry->shift, entry->mask));
    print_name_value(entry->name, ss_to_string(&ss));
    ss_destroy(&ss);
}

static void print_all_registers(void)
{
    sstream_s ss;
    ss_init(&ss);
    ss_printf(&ss, "--- Control Status Registers ---\n");
    ss_printf(&ss, "       PC:  0x%08x       FCSR:  0x%08x  ", PC, C[CSR_FCSR]);
    ss_printf(&ss, "   FFLAGS:  0x%08x        FRM:  0x%08x\n", GET_FFLAGS, GET_FRM);
    ss_printf(&ss, "  MCYCLEH:  0x%08x     MCYCLE:  0x%08x  ", C[CSR_MCYCLEH], C[CSR_MCYCLE]);
    ss_printf(&ss, "MINSTRETH:  0x%08x   MINSTRET:  0x%08x\n", C[CSR_MINSTRETH], C[CSR_MINSTRET]);
    ss_printf(&ss, "   MTIMEH:  0x%08x      MTIME:  0x%08x  ", C[CSR_MTIMEH], C[CSR_MTIME]);
    ss_printf(&ss, "MTIMECMPH:  0x%08x   MTIMECMP:  0x%08x\n", C[CSR_MTIMECMPH], C[CSR_MTIMECMP]);
    ss_printf(&ss, " MSTATUSH:  0x%08x    MSTATUS:  0x%08x  ", C[CSR_MSTATUSH], C[CSR_MSTATUS]);
    ss_printf(&ss, "      MIE:  0x%08x        MIP:  0x%08x\n", C[CSR_MIE], C[CSR_MIP]);
    ss_printf(&ss, "   MCAUSE:  0x%08x       MEPC:  0x%08x  ", C[CSR_MCAUSE], C[CSR_MEPC]);
    ss_printf(&ss, "    MTVEC:  0x%08x   MSCRATCH:  0x%08x\n", C[CSR_MTVEC], C[CSR_MSCRATCH]);
    ss_printf(&ss, "    MTVAL:  0x%08x\n", C[CSR_MTVAL]);

    ss_printf(&ss, "-- Integer Registers ---");
    for (int i = 0; i < R_LENGTH; ++i) {
        if (!(i & 3))
            ss_printf(&ss, "\n");
        else
            ss_printf(&ss, "  ");
        ss_printf(&ss, "     %4s:  0x%08x", fe_gpr_names[i], R[i]);
    }
    ss_printf(&ss, "\n");

    ss_printf(&ss, "--- Floating-Point Registers ---");
    sstream_s fpss;
    ss_init(&fpss);
    const int fp_spacing = 17;
    int prev_fplen = fp_spacing;
    for (int i = 0; i < FR_LENGTH; ++i) {
        if (!(i & 3)) {
            ss_printf(&ss, "\n     ");
            prev_fplen = fp_spacing;    // To force 0 prefix spacing
        }

        // Calculate spacing required, since previous fp has arbitary length
        ss_printf(&fpss, "%f", FR[i]);
        int prefix_space = fp_spacing - prev_fplen;
        char *fpstr = ss_to_string(&fpss);

        ss_printf(&ss, "%*s%4s:  %s", prefix_space, "", fe_fpr_names[i], fpstr);

        prev_fplen = strlen(fpstr);
        ss_clear(&fpss);
    }
    ss_destroy(&fpss);
    ss_printf(&ss, "\n");

    char *str = ss_to_string(&ss);
    fe->write_log(0, "%s", str);
    free(str);
}

/* Print name-value pairs with consistent spacing */
static void print_name_value(char *name, char *value)
{
    fe->write_log(0, "%9s:  %s\n", name, value);
}

/* Choose the proper printing function for the given token */
static void print_cmdtok(cmdtok_s *tok)
{
    switch (tok->type) {
        case CMD_REG:
            print_register(tok->val.i);
            break;
        case CMD_CSR:
            print_csr(tok->val.i);
            break;
        case CMD_INT:
        case CMD_PC:    /* Normal printing does not treat PC as special case */
            print_memory_line(tok->val.u);
            break;
        default: {}
    }
}

/* Print all of the entries from the watch list */
static void print_watch_list(void)
{
    cmdtoklist_s *iter = watch_list;
    while (iter) {
        if (iter->tok.type == CMD_PC)
            /* Watching PC always prints upcoming instruction */
            print_memory_line(PC);
        else
            print_cmdtok(&iter->tok);
        iter = iter->next;
    }
}

/* Append an arbitrary token's name or string representation to sstream */
static void format_cmdtok(sstream_s *ss, cmdtok_s *tok)
{
    switch (tok->type) {
        case CMD_CHAR:
            ss_printf(ss, "%c", tok->val.i);
            break;
        case CMD_INT:
            ss_printf(ss, "0x%08x", tok->val.u);
            break;
        case CMD_PC:
            ss_printf(ss, "pc");
            break;
        case CMD_REG:
            if (tok->val.u & FREG_BIT)
                ss_printf(ss, fe_fpr_names[tok->val.u - FREG_BIT]);
            else
                ss_printf(ss, fe_gpr_names[tok->val.u]);
            break;
        case CMD_CSR:
            {
                csr_encoding_s *entry = SEARCH_CSR_ENCODING(tok->val.u);
                ss_printf(ss, "%s", entry ? entry->name : "<unknown csr>");
            }
            break;
        case CMD_STR:
            ss_printf(ss, "%s", tok->val.str);
            break;
        default:
            ss_printf(ss, "<unknown>");
    }
}


/* ========================================================================== *
 * Dump Functions
 * ========================================================================== */

static int format_memory_line(sstream_s *ss, memaddr_t addr)
{
    /* Dependending on the segment (text vs data), print either 1 instruction,
     * or 16 data bytes followed by their ASCII translation */
    if (is_text_seg(segment_index_from_addr(addr, 1))) {
        char *inst_str = inst_to_string_at_addr(addr);
        if (!inst_str)
            return 0;
        ss_printf(ss, "%s\n", inst_str);
        free(inst_str);
        return BP_WORD;

    } else {
        /* Determine data width, defaults to byte */
        int data_width = BP_BYTE;
        if (addr_in_tty_range(addr, BP_WORD)) {
            /* @NOTE: Will need special handling for different device-specific
             *   memory-widths.
             * @TEMP: For now, hard-coded for word-aligned TTY. */
            data_width = BP_WORD;
        }

        /* Align memory address down to a quad-word */
        memaddr_t a = ALIGN_DOWN(addr, BP_QWORD);
        ss_printf(ss, "[0x%08x] ", a);

        /* Print data in hexa */
        unsigned char line[BP_QWORD];
        uint16_t *line16 = (uint16_t *)line;
        uint32_t *line32 = (uint32_t *)line;
        ctx.exception_occurred = false;
        for (int i = 0; i < BP_QWORD / data_width; i++, a += data_width) {
            switch (data_width) {
                case BP_BYTE:
                    line[i] = read_byte(a, EMU_ACCESS);
                    if (ctx.exception_occurred)
                        return 0;
                    ss_printf(ss, " %02x", line[i]);
                    break;
                case BP_HALF:
                    line16[i] = read_half(a, EMU_ACCESS);
                    if (ctx.exception_occurred)
                        return 0;
                    ss_printf(ss, " %04x", line16[i]);
                    break;
                case BP_WORD:
                    line32[i] = read_word(a, EMU_ACCESS);
                    if (ctx.exception_occurred)
                        return 0;
                    ss_printf(ss, " %08x", line32[i]);
                    break;
            }

        }

        /* Print bytes in ASCII */
        ss_printf(ss, "  ");
        for (int i = 0; i < BP_QWORD; ++i)
            ss_print_byte(ss, line[i]);
        ss_printf(ss, "\n");

        return BP_QWORD;
    }
}

static void format_memory_range(sstream_s *ss, memaddr_t lo, memaddr_t hi_ex)
{
    while (lo < hi_ex) {
        lo += format_memory_line(ss, lo);
    }
}

static void format_empty_memory_range(sstream_s *ss, memaddr_t lo, memaddr_t hi_ex)
{
    if (lo < hi_ex)
        ss_printf(ss, "[0x%08x]...[0x%08x] 0x00000000\n", lo, hi_ex - 1);
}

static void dump_program(void)
{
    memaddr_t lo, hi;
    sstream_s ss;
    ss_init(&ss);

    ss_printf(&ss, "--- TEXT ---\n");
    lo = ctx.program_addr;
    hi = M[TEXT_SEG].loc_counter;
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[TEXT_SEG].upper_bound);

    // Need to ensure quadword-alignment in data segment memory lines
    ss_printf(&ss, "--- DATA ---\n");
    lo = M[SMALL_DATA_SEG].lower_bound & ~(BP_QWORD - 1);
    hi = (M[SMALL_DATA_SEG].loc_counter + BP_QWORD - 1) & ~(BP_QWORD - 1);
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[SMALL_DATA_SEG].upper_bound);
    lo = M[DATA_SEG].lower_bound & ~(BP_QWORD - 1);
    hi = (M[DATA_SEG].loc_counter + BP_QWORD - 1) & ~(BP_QWORD - 1);
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[DATA_SEG].upper_bound);

    char *str = ss_to_string(&ss);
    fe->write_output("%s", str);
    free(str);
}

static void full_dump_program(void)
{
    memaddr_t lo, hi;
    sstream_s ss;
    ss_init(&ss);

    ss_printf(&ss, "--- TEXT ---\n");
    lo = M[TEXT_SEG].lower_bound;
    hi = M[TEXT_SEG].loc_counter;
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[TEXT_SEG].upper_bound);

    // Need to ensure quadword-alignment in data segment memory lines
    ss_printf(&ss, "--- DATA ---\n");
    lo = M[SMALL_DATA_SEG].lower_bound & ~(BP_QWORD - 1);
    hi = (M[SMALL_DATA_SEG].loc_counter + BP_QWORD - 1) & ~(BP_QWORD - 1);
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[SMALL_DATA_SEG].upper_bound);
    lo = M[DATA_SEG].lower_bound & ~(BP_QWORD - 1);
    hi = (M[DATA_SEG].loc_counter + BP_QWORD - 1) & ~(BP_QWORD - 1);
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[DATA_SEG].upper_bound);

    ss_printf(&ss, "--- STACK ---\n");
    lo = R[REG_SP] & ~(BP_QWORD - 1);
    hi = M[STACK_SEG].upper_bound;
    format_empty_memory_range(&ss, M[STACK_SEG].lower_bound, lo);
    format_memory_range(&ss, lo, hi);

    ss_printf(&ss, "--- KERNEL TEXT ---\n");
    lo = M[K_TEXT_SEG].lower_bound;
    hi = M[K_TEXT_SEG].loc_counter;
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[K_TEXT_SEG].upper_bound);

    // Need to ensure quadword-alignment in data segment memory lines
    ss_printf(&ss, "--- KERNEL DATA ---\n");
    lo = M[K_DATA_SEG].lower_bound & ~(BP_QWORD - 1);
    hi = (M[K_DATA_SEG].loc_counter + BP_QWORD - 1) & ~(BP_QWORD - 1);
    format_memory_range(&ss, lo, hi);
    format_empty_memory_range(&ss, hi, M[K_DATA_SEG].upper_bound);

    char *str = ss_to_string(&ss);
    fe->write_output("%s", str);
    free(str);
}
